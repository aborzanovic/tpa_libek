/**
 * Script configuration.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

module.exports = {
  /**
   * The root path to all scripts.
   *
   * @type {String}
   */
  source: './resources/assets/scripts',

  /**
   * The path where the scripts should be written.
   *
   * @type {String}
   */
  destination: './public/scripts',

  /**
   * The scripts which should be linted.
   *
   * @type {Array}
   */
  linting: [
    './resources/assets/scripts/**/*.js',
    '!./resources/assets/scripts/vendor/**/*.js'
  ],

  /**
   * The root path of all vendor scripts.
   *
   * @type {String}
   */
  vendorRoot: './resources/assets/scripts/vendor',

  /**
   * All plugins to be compiled.
   *
   * @type {Array}
   */
  plugins: {
    admin: [
      'polyfills/**/*.js',
      'creitive/jquery.helperPlugins/jquery.helperPlugins.js',
      'enyo/dropzone/dropzone.js',
      'imperavi/redactor/redactor.js',
      'twbs/bootstrap/bootstrap.js',
      'nakupanda/bootstrap3-dialog/bootstrap-dialog.js',
      'dbushell/Nestable/jquery.nestable.js',
      'carhartl/jquery-cookie/jquery.cookie.js',
      'malihu/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js',
      'mattbryson/TouchSwipe-Jquery-Plugin/jquery.touchSwipe.js'
    ],
    login: [
      'polyfills/**/*.js',
      'creitive/jquery.helperPlugins/jquery.helperPlugins.js',
      'twbs/bootstrap/bootstrap.js',
      'carhartl/jquery-cookie/jquery.cookie.js',
      'nosilleg/capslockstate-jquery-plugin/jquery.capslockstate.js',
      'jaz303/tipsy/jquery.tipsy.js'
    ],
    front: [
      'polyfills/**/*.js',
      'creitive/jquery.helperPlugins/jquery.helperPlugins.js',
      'twbs/bootstrap/bootstrap.js',
      'nakupanda/bootstrap3-dialog/bootstrap-dialog.js',
      'OwlFonk/OwlCarousel/owl.carousel.js',
      'carhartl/jquery-cookie/jquery.cookie.js'
    ]
  }
};
