/**
 * Project-specific configuration for gulp.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

module.exports = require('require-dir')();
