/**
 * Image optimization configuration.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

module.exports = {
  source: [
    './public/**/*'
  ],
  destination: './public'
};
