'use strict';

var fs = require('fs');

/**
 * Finds all filenames within the source path and passes them to the callback.
 *
 * @param {String} source
 * @param {Function} callback
 * @return {Void}
 */
module.exports = function(source, callback) {
  fs.readdir(source, function(err, files) {
    files.forEach(function(file) {
      fs.stat(source + '/' + file, function(err, stats) {
        if (stats.isFile()) {
          callback(file);
        }
      });
    });
  });
};
