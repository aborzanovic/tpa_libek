'use strict';

/**
 * Plugin dependencies.
 */

var gulp = require('gulp');
var watchify = require('gulp-watchify');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var plumber = require('gulp-plumber');
var browserifyShim = require('browserify-shim');
var aliasify = require('aliasify');
var hbsfy = require('hbsfy');
var streamify = require('gulp-streamify');
var normalizeAliasifyConfig = require('./normalizeAliasifyConfig');

/**
 * Watches scripts and autorebundles them.
 *
 * @param {String} name
 * @param {String} source
 * @param {String} destination
 * @param {Object} aliasifyConfig
 * @return {Function}
 */
module.exports = function(name, source, destination, aliasifyConfig) {
  watchify(function(watchify) {
    var bundlePaths = source + '/' + name + '.js';
    var minifiedFilename = name + '.min.js';

    return gulp.src(bundlePaths)
      .pipe(plumber())
      .pipe(watchify({
        watch: true,
        /**
         * Sets up the bundle by applying some transforms.
         *
         * @param {Browserify} bundle
         * @return {Browserify}
         */
        setup: function(bundle) {
          bundle
            .transform(hbsfy)
            .transform(aliasify.configure(normalizeAliasifyConfig(aliasifyConfig)))
            .transform(browserifyShim);
        }
      }))
      .pipe(gulp.dest(destination))
      .pipe(streamify(uglify()))
      .pipe(rename(minifiedFilename))
      .pipe(gulp.dest(destination));
  })();
};
