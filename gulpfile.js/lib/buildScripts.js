'use strict';

/**
 * Plugin dependencies.
 */

var gulp = require('gulp');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var browserify = require('browserify');
var browserifyShim = require('browserify-shim');
var aliasify = require('aliasify');
var hbsfy = require('hbsfy');
var vinylSourceStream = require('vinyl-source-stream');
var streamify = require('gulp-streamify');
var normalizeAliasifyConfig = require('./normalizeAliasifyConfig');

/**
 * Builds the custom website scripts.
 *
 * @param {String} name
 * @param {String} source
 * @param {String} destination
 * @param {Object} aliasifyConfig
 * @return {Void}
 */
module.exports = function(name, source, destination, aliasifyConfig) {
  var sourceFilename = source + '/' + name + '.js';
  var completeFilename = name + '.js';
  var minifiedFilename = name + '.min.js';

  browserify(sourceFilename)
    .transform(hbsfy)
    .transform(aliasify.configure(normalizeAliasifyConfig(aliasifyConfig)))
    .transform(browserifyShim)
    .bundle()
    .pipe(vinylSourceStream(completeFilename))
    .pipe(gulp.dest(destination))
    .pipe(streamify(uglify()))
    .pipe(rename(minifiedFilename))
    .pipe(gulp.dest(destination));
};
