'use strict';

var gulp = require('gulp');
var optimizeImages = require('../lib/optimizeImages');
var config = require('../config').optimizeImages;

gulp.task('optimize-images', function() {
  optimizeImages(config.source, config.destination);
});
