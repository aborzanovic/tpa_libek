<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table)
        {
            /*
             * Auto-incrementing primary key.
             */
            $table->increments('id');

            /*
             * JSON-encoded data submitted along with the form.
             */
            $table->text('data');

            /*
             * Timestamps.
             */
            $table->timestamp('submitted_at')->nullable();
            $table->timestamps();

            /*
             * Soft deletion.
             */
            $table->softDeletes();

            /*
             * MySQL storage engine.
             */
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('applications');
    }
}
