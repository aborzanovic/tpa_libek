<?php

namespace App\ViewData\Middleware;

use Cartalyst\Sentinel\Sentinel;
use Closure;
use Creitive\ViewData\ViewData;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

class AttachSessionDependentDataToViewData
{
    /**
     * The view factory implementation.
     *
     * @var \Illuminate\Contracts\View\Factory
     */
    protected $viewData;

    /**
     * A Sentinel instance.
     *
     * @var \Cartalyst\Sentinel\Sentinel
     */
    protected $sentinel;

    /**
     * Create a new error binder instance.
     *
     * @param \Creitive\ViewData\ViewData $viewData
     * @param \Cartalyst\Sentinel\Sentinel $sentinel
     * @return void
     */
    public function __construct(ViewData $viewData, Sentinel $sentinel)
    {
        $this->viewData = $viewData;
        $this->sentinel = $sentinel;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $this->viewData->currentUser = $this->sentinel->getUser();
        $this->viewData->successMessages = $request->session()->get('successMessages', new MessageBag);

        return $next($request);
    }
}
