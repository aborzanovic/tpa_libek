<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Front\Controller;

class HomeController extends Controller
{
    /**
     * Displays the website home page.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('home');
    }
}
