<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Controller;

class HomeController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->viewData->bodyDataPage = 'admin-home';
        $this->viewData->pageTitle->setPage(trans('common.adminPanel'));
    }

    /**
     * Gets the admin panel homepage.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.home');
    }
}
