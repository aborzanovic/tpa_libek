<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller as BaseController;
use URL;

class Controller extends BaseController
{
    public function __construct()
    {
        parent::__construct();

        $this->buildNavigation();
    }

    /**
     * Sets the base assets (scripts and styles).
     *
     * @return void
     */
    public function setBaseAssets()
    {
        $this->assets->loadAssetsForAdmin();
    }

    /**
     * Builds the admin panel navigation.
     *
     * @return void
     */
    protected function buildNavigation()
    {
        $navigation = $this->viewData->navigation->get('admin.main');
        $currentUser = $this->viewData->currentUser;

        $navigation->addLast(
            'home',
            [
                'href' => URL::action('App\Http\Controllers\Admin\HomeController@index'),
                'icon' => 'home',
                'label' => trans('admin/navigation.home'),
            ]
        );

        if ($currentUser && $currentUser->hasAccess('App\Auth\Http\Controllers\Admin\User\Controller@index')) {
            $navigation->addLast(
                'users',
                [
                    'href' => URL::action('App\Auth\Http\Controllers\Admin\User\Controller@index'),
                    'icon' => 'users',
                    'label' => trans('admin/navigation.users'),
                ]
            );
        }

        $navigation->addLast(
            'password',
            [
                'href' => URL::action('App\Auth\Http\Controllers\Admin\Password\Controller@index'),
                'icon' => 'lock',
                'label' => trans('admin/navigation.password'),
            ]
        );

        $navigation->addLast(
            'back-to-website',
            [
                'href' => URL::action('App\Http\Controllers\Front\HomeController@index'),
                'icon' => 'globe',
                'label' => trans('admin/navigation.backToWebsite'),
            ]
        );

        $navigation->setActive('home');
    }
}
