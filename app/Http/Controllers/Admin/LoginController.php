<?php namespace App\Http\Controllers\Admin;

use App\Auth\Login\Manager as LoginManager;
use App\Http\Controllers\Controller as BaseController;
use AssetManager;
use Illuminate\Http\Request;
use Redirect;
use URL;

class LoginController extends BaseController
{
    /**
     * The current request instance.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * A LoginManager instance.
     *
     * @var \App\Auth\Login\Manager
     */
    protected $loginManager;

    public function __construct(Request $request, LoginManager $loginManager)
    {
        parent::__construct();

        $this->request = $request;
        $this->loginManager = $loginManager;

        $this->viewData->bodyDataPage = 'admin-login';
        $this->viewData->pageTitle->setPage('Admin login');
    }

    /**
     * Gets the login form.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.login');
    }

    /**
     * Attempts to log the user in, and redirects them to the admin panel home
     * page if successful, or back to the login form otherwise.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login()
    {
        if ($this->loginManager->login($this->request->all())) {
            $default = URL::action('App\Http\Controllers\Admin\HomeController@index');

            return Redirect::intended($default);
        }

        return Redirect::action('App\Http\Controllers\Admin\LoginController@index')
            ->withErrors($this->loginManager->getErrors())
            ->withInput();
    }

    /**
     * Logs the user out, and redirects them back to the login form.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        $this->loginManager->logout();

        return Redirect::action('App\Http\Controllers\Admin\LoginController@index')->with('loggedOut', true);
    }

    /**
     * Sets the base assets (scripts and styles).
     *
     * @return void
     */
    public function setBaseAssets()
    {
        $this->assets->loadAssetsForAdminLogin();
    }
}
