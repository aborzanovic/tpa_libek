<?php

namespace App\Http\Middleware;

use Cartalyst\Sentinel\Sentinel;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class RedirectIfAuthenticated
{
    /**
     * The Sentinel instance.
     *
     * @var \Cartalyst\Sentinel\Sentinel
     */
    protected $sentinel;

    /**
     * A Redirector instance.
     *
     * @var \Illuminate\Routing\Redirector
     */
    protected $redirect;

    /**
     * Create a new middleware instance.
     *
     * @param \Cartalyst\Sentinel\Sentinel $sentinel
     * @return void
     */
    public function __construct(Sentinel $sentinel, Redirector $redirect)
    {
        $this->sentinel = $sentinel;
        $this->redirect = $redirect;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($this->sentinel->check()) {
            return $this->redirect->action('Admin\HomeController@index');
        }

        return $next($request);
    }
}
