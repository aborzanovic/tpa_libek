<?php

namespace App\Providers;

use Creitive\Module\ServiceProvider;
use DaveJamesMiller\Breadcrumbs\Manager as BreadcrumbManager;
use DaveJamesMiller\Breadcrumbs\Generator as BreadcrumbGenerator;
use Illuminate\Contracts\Routing\Registrar as RegistrarContract;
use Illuminate\Routing\Router;
use Lang;
use URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function boot()
    {
        $this->registerRoutePatterns($this->app['router']);
        $this->registerHomepageRoutes($this->app['router']);
        $this->registerAdminLoginRoutes($this->app['router']);
        $this->registerAdminRoutes($this->app['router']);
        $this->registerAdminBreadcrumbs($this->app['breadcrumbs']);
    }

    /**
     * {@inheritDoc}
     */
    public function register()
    {
    }

    /**
     * Registers default application route patterns.
     *
     * @param \Illuminate\Routing\Router $router
     * @return void
     */
    protected function registerRoutePatterns(Router $router)
    {
        $anyRegex = $this->routePatternRegexes['any'];
        $slugRegex = $this->routePatternRegexes['slug'];
        $yearRegex = $this->routePatternRegexes['year'];
        $monthRegex = $this->routePatternRegexes['month'];
        $dayRegex = $this->routePatternRegexes['day'];

        /*
         * Matches any route, with an arbitrary depth level. Note that this
         * route should be declared last within a certain route prefix, because
         * it will match everything that hasn't been matched by a previously
         * defined route.
         */
        $router->pattern('any', $anyRegex);

        /*
         * Slug pattern definition.
         */
        $router->pattern('slug', $slugRegex);

        /*
         * Date pattern definitions.
         */
        $router->pattern('year', $yearRegex);
        $router->pattern('month', $monthRegex);
        $router->pattern('day', $dayRegex);
    }

    /**
     * Registers the home page and the language redirect routes.
     *
     * @param \Illuminate\Contracts\Routing\Registrar $router
     * @return void
     */
    protected function registerHomepageRoutes(RegistrarContract $router)
    {
        /*
         * Website homepage route.
         */
        $router->get('', 'App\Http\Controllers\Front\HomeController@index');
    }

    /**
     * Registers routes for the admin panel login.
     *
     * @param \Illuminate\Contracts\Routing\Registrar $router
     * @return void
     */
    protected function registerAdminLoginRoutes(RegistrarContract $router)
    {
        $attributes = [
            'prefix' => 'admin',
            'middleware' => 'guest',
            'namespace' => 'App\Http\Controllers\Admin',
        ];

        $router->group($attributes, function (RegistrarContract $router) {
            /*
             * Admin panel login routes.
             */
            $router->get('login', 'LoginController@index');
            $router->post('login', 'LoginController@login');
        });
    }

    /**
     * Registers some basic admin panel routes.
     *
     * @param \Illuminate\Contracts\Routing\Registrar $router
     * @return void
     */
    protected function registerAdminRoutes(RegistrarContract $router)
    {
        $attributes = [
            'prefix' => 'admin',
            'middleware' => ['auth', 'permissions'],
            'namespace' => 'App\Http\Controllers\Admin',
        ];

        $router->group($attributes, function (RegistrarContract $router) {
            /*
             * Admin panel home page.
             */
            $router->get('', 'HomeController@index');

            /*
             * Admin panel logout route.
             */
            $router->post('logout', 'LoginController@logout');
        });

        /*
         * The route for changing the password.
         */
        $attributes = [
            'prefix' => 'admin/password',
            'middleware' => ['auth', 'permissions'],
            'namespace' => 'App\Auth\Http\Controllers\Admin\Password',
        ];

        $router->group($attributes, function (RegistrarContract $router) {
            $router->get('', 'Controller@index');
            $router->put('', 'Controller@update');
        });
    }

    /**
     * Registers default admin breadcrumbs.
     *
     * @param \DaveJamesMiller\Breadcrumbs\Manager $breadcrumbs
     * @return void
     */
    protected function registerAdminBreadcrumbs(BreadcrumbManager $breadcrumbs)
    {
        /*
         * Admin homepage breadcrumb.
         */
        $breadcrumbs->register('admin::home', function (BreadcrumbGenerator $breadcrumbs) {
            $url = URL::action('App\Http\Controllers\Admin\HomeController@index');

            $breadcrumbs->push(Lang::get('admin/navigation.home'), $url);
        });

        /*
         * Password change breadcrumb.
         */
        $breadcrumbs->register('admin::password', function (BreadcrumbGenerator $breadcrumbs) {
            $breadcrumbs->parent('admin::home');

            $url = URL::action('App\Auth\Http\Controllers\Admin\Password\Controller@index');

            $breadcrumbs->push(Lang::get('admin/navigation.password'), $url);
        });
    }
}
