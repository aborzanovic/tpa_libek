<?php

namespace App\Validation;

/*
 * Some might consider this a particularly awful class. We can neither confirm
 * nor deny that claim.
 */

use DateTime;
use DB;
use Exception;
use Illuminate\Validation\Validator as BaseValidator;
use Imagick;
use Str;

class Validator extends BaseValidator
{
    /**
     * Validates whether a slug is valid.
     *
     * The constraints are mostly related to the allowed characters - a slug may
     * only have lowercase ASCII letters, numbers, and dashes, must not start or
     * end with a dash, and must not have two consecutive dashes.
     *
     * @param string $attribute
     * @param string $slug
     * @return bool
     */
    public function validateSlug($attribute, $slug)
    {
        return $slug === Str::slug($slug);
    }

    /**
     * Tests whether a given string is a valid integer, meaning it only contains
     * actual digits, and nothing else.
     *
     * This is different from the `numeric` and `integer` validation rules.
     *
     * @param string $attribute
     * @param mixed $input
     * @return boolean
     */
    public function validateDigitsOnly($attribute, $input)
    {
        /**
         * This pattern only allows digits in the string, and nothing else, and
         * requires at least one digit present.
         *
         * @var string
         */
        $pattern = '/^\d+$/D';

        return (bool) preg_match($pattern, $input);
    }

    /**
     * Validates that a date is correct according to the provided format.
     *
     * @param string $attribute
     * @param string $date
     * @param array  $parameters
     * @return boolean
     */
    public function validateCustomDateFormat($attribute, $date, array $parameters = array())
    {
        $format = isset($parameters[0]) ? $parameters[0] : 'Y-m-d';

        return (
            ($datetime = DateTime::createFromFormat($format, $date))
            && ($datetime->format($format) === $date)
        );
    }

    /**
     * Validates that the provided password matches the one belonging to the
     * current user.
     *
     * Expects to have 'current_user' in the input data.
     *
     * @param string $attribute
     * @param string $password
     * @return boolean
     */
    public function validateCurrentPassword($attribute, $password)
    {
        $hasher = $this->container->make('sentinel.hasher');

        return $hasher->check($password, $this->data['real_current_password']);
    }

    /**
     * Validates that the passed value is an existing page ID.
     *
     * @param string $attribute
     * @param mixed $parentId
     * @param array $parameters
     * @return boolean
     */
    public function validateParentId($attribute, $parentId, $parameters)
    {
        $this->requireParameterCount(1, $parameters, 'parent_id');

        $collectionName = $parameters[0];

        $parentId = (int) $parentId;

        if ($parentId === 0) {
            return true;
        }

        $ids = $this->data[$collectionName]->modelKeys();

        return in_array($parentId, $ids, true);
    }

    /**
     * Validates that the provided template is valid.
     *
     * The `templates` must be added to the input data.
     *
     * @param string $attribute
     * @param mixed $templateId
     * @return boolean
     */
    public function validateTemplate($attribute, $templateId)
    {
        return array_key_exists($templateId, $this->data['templates']);
    }

    /**
     * Validates that the provided value is not a specific ID.
     *
     * Safer than using `not_in` because it casts to `integer`.
     *
     * @param string $attribute
     * @param mixed $input
     * @param array $parameters
     * @return boolean
     */
    public function validateNotId($attribute, $input, $parameters)
    {
        $this->requireParameterCount(1, $parameters, 'not_id');

        $input = (int) $input;
        $id = (int) $parameters[0];

        return ($input !== $id);
    }

    /**
     * Validates whether the input is a valid latitude.
     *
     * @param string $attribute
     * @param mixed $input
     * @return boolean
     */
    public function validateLatitude($attribute, $input)
    {
        $regex = '/^'           // String start
                . '\-?'         // An optional minus in front
                . '\d{1,2}'     // 1 or 2 digits
                . '(\.\d+)?'    // An optional dot, followed by at least 1 digit
                . '$/D';        // String end

        if (!preg_match($regex, $input)) {
            return false;
        }

        $input = (int) $input;

        return (abs($input) <= 90);
    }

    /**
     * Validates whether the input is a valid longitude.
     *
     * @param string $attribute
     * @param mixed $input
     * @return boolean
     */
    public function validateLongitude($attribute, $input)
    {
        $regex = '/^'           // String start
                . '\-?'         // An optional minus in front
                . '\d{1,3}'     // 1 to 3 digits
                . '(\.\d+)?'    // An optional dot, followed by at least 1 digit
                . '$/D';        // String end

        if (!preg_match($regex, $input)) {
            return false;
        }

        $input = (int) $input;

        return (abs($input) <= 180);
    }

    /**
     * Validates a unique combination of values.
     *
     * Taken from the linked SO answer and modified a bit.
     *
     * Should be used like this:
     *
     *     Validator::make(
     *         [
     *             'any_name' => ['value1', 'value2', 'value3'],
     *         ],
     *         [
     *             'any_name' => ['unique_combination:table_name,column1,column2,column3'],
     *         ]
     *     );
     *
     * This will pass only if `table_name` doesn't contain a row with columns
     * `column1`, `column2`, and `column3` having values `value1`, `value2`, and
     * `value3`, respectively.
     *
     * @link http://stackoverflow.com/a/26684043
     * @param string $attribute
     * @param mixed $value
     * @param array $parameters
     * @return boolean
     */
    public function validateUniqueCombination($attribute, $value, $parameters)
    {
        // Get table name from first parameter
        $table = array_shift($parameters);

        // Build the query
        $query = DB::table($table);

        // Add the field conditions
        foreach ($parameters as $i => $field) {
            $query->where($field, $value[$i]);
        }

        // Validation result will be false if any rows match the combination
        return (!$query->exists());
    }

    /**
     * Validates that the input is a properly base64-encoded image.
     *
     * @param string $attribute
     * @param mixed $value
     * @param array $parameters
     * @return boolean
     */
    public function validateBase64EncodedImage($attribute, $value, $parameters)
    {
        $imageBlob = base64_decode($value, true);

        if (!$imageBlob) {
            return false;
        }

        try {
            $image = new Imagick;
            $image->readImageBlob($imageBlob);

            return $image->valid();
        } catch (Exception $exception) {
            return false;
        }
    }
}
