<?php

namespace App\Redactor\Http\Controllers\Admin\File;

use App\Http\Controllers\Admin\Controller as BaseController;
use App\Redactor\Http\Requests\Admin\File\StoreRequest;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Controller extends BaseController
{
    /**
     * The path to which the files will be uploaded.
     *
     * @var string
     */
    protected $uploadPath;

    public function __construct($uploadPath = '/upload/files/')
    {
        $this->uploadPath = $uploadPath;
    }

    /**
     * Uploads a file to the server, and returns its location.
     *
     * Returns a JSON response containing the file's URL in a `filelink`
     * attribute, and its name in a `filename` attribute, which is what Redactor
     * expects.
     *
     * @param \App\Redactor\Http\Requests\Admin\File\StoreRequest $request
     * @return array
     */
    public function store(StoreRequest $request)
    {
        $file = $request->file('file');

        $parameters = $this->getDestination($file);

        $file->move($parameters['destination'], $parameters['name']);

        return [
            'filelink' => $parameters['fileHref'],
            'filename' => $file->getClientOriginalName(),
        ];
    }

    /**
     * Generates a new filename for uploading files.
     *
     * Returns an array with the following keys:
     *
     * - `destination`: the path where the file should be moved
     * - `name`: the name that should be given to the file
     * - `fileHref`: the URL of the file on the server (relative to the domain
     *    root)
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     * @return array
     */
    protected function getDestination(UploadedFile $file)
    {
        $randomFileName = time() . '.' . str_random(16) . '.' . $file->guessExtension();

        return [
            'destination' => public_path() . $this->uploadPath,
            'name' => $randomFileName,
            'fileHref' => $this->uploadPath . $randomFileName,
        ];
    }
}
