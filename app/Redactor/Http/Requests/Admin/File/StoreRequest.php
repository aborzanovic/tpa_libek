<?php

namespace App\Redactor\Http\Requests\Admin\File;

use App\Http\Requests\Request;

class StoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'file' => ['required', 'mimes:pdf,doc,docx,xls,xlsx,odt,ods,txt,csv,zip,rar,tar,gz,tgz', 'max:10240'],
        ];

        return $rules;
    }
}
