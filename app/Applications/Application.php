<?php

namespace App\Applications;

use Creitive\Database\Eloquent\Model;
use Creitive\Models\Traits\CalcFoundRowableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Application extends Model
{
    use CalcFoundRowableTrait;
    use SoftDeletes;

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'array',
    ];
}
