<?php

namespace App\Applications\Http\Middleware;

use App\AssetManager\AssetManager;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;

class PreventAccessIfDeadlineHasExpired
{
    /**
     * The application deadline.
     *
     * @var \Carbon\Carbon
     */
    protected $deadline;

    /**
     * An AssetManager instance.
     *
     * @var \App\AssetManager\AssetManager
     */
    protected $assetManager;

    /**
     * Create a new middleware instance.
     *
     * @param \Carbon\Carbon $deadline
     * @return void
     */
    public function __construct(Carbon $deadline, AssetManager $assetManager)
    {
        $this->deadline = $deadline;
        $this->assetManager = $assetManager;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($this->deadline->isPast()) {
            $this->assetManager->loadAssetsForFront();

            return view('applications.expired');
        }

        return $next($request);
    }
}
