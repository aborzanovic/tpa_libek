<?php

namespace App\Applications\Application;

use App\Applications\Application;
use App\Applications\Application\Repository as ApplicationRepository;
use App\Applications\Http\Middleware\PreventAccessIfDeadlineHasExpired;
use App\Applications\Listeners\SendApplicationToAdmins;
use App\AssetManager\AssetManager;
use Carbon\Carbon;
use Creitive\Module\ServiceProvider as BaseServiceProvider;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Routing\Registrar as RegistrarContract;
use Illuminate\Routing\Router;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function boot()
    {
        $this->registerRoutePatterns($this->app['router'], $this->app);
        $this->registerFrontRoutes($this->app['router']);
    }

    /**
     * {@inheritDoc}
     */
    public function register()
    {
        $this->app->singleton(PreventAccessIfDeadlineHasExpired::class, function ($app) {
            return new PreventAccessIfDeadlineHasExpired(
                new Carbon($app['config']->get('applications.deadline')),
                $app[AssetManager::class]
            );
        });

        $this->app->singleton(ApplicationRepository::class, function ($app) {
            return new ApplicationRepository(
                new Application,
                $app['filesystem']->drive('local')
            );
        });

        $this->app->singleton(SendApplicationToAdmins::class, function ($app) {
            return new SendApplicationToAdmins(
                $app['filesystem']->drive('local'),
                $app['mailer']
            );
        });
    }

    /**
     * Registers the route patterns.
     *
     * @param \Illuminate\Routing\Router $router
     * @param \Illuminate\Contracts\Container\Container $container
     * @return void
     */
    protected function registerRoutePatterns(Router $router, Container $container)
    {
        $idRegex = $this->routePatternRegexes['id'];

        $router->pattern('application', $idRegex);

        $router->bind('application', function ($value) use ($container) {
            return Application::findOrFail($value);
        });
    }

    /**
     * Registers front-end routes.
     *
     * @param \Illuminate\Contracts\Routing\Registrar $router
     * @return void
     */
    protected function registerFrontRoutes(RegistrarContract $router)
    {
        $attributes = [
            'prefix' => 'prijava',
            'middleware' => ['deadline'],
            'namespace' => 'App\Applications\Http\Controllers\Front\Application',
        ];

        $router->group($attributes, function (RegistrarContract $router) {
            $router->get('', 'Controller@index');
            $router->post('', ['middleware' => 'throttle:10,10', 'uses' => 'Controller@store']);
        });
    }
}
