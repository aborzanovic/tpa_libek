<?php

namespace App\Applications\Events;

use App\Applications\Application;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class ApplicationWasSubmitted extends Event
{
    use SerializesModels;

    /**
     * An Application instance.
     *
     * @var \App\Applications\Application
     */
    public $application;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
