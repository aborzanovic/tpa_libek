<?php

namespace App\Auth;

use Cartalyst\Sentinel\Users\EloquentUser;
use Creitive\Models\Traits\CalcFoundRowableTrait;
use Illuminate\Database\Eloquent\Builder;

class User extends EloquentUser
{
    use CalcFoundRowableTrait;

    protected $visible = [
        'first_name',
        'last_name',
        'email',
        'created_at',
        'updated_at',
    ];

    /**
     * Limits the query to users that don't have roles attached.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithoutRoles(Builder $query)
    {
        return $query->has('roles', '=', 0);
    }

    /**
     * Gets the user's full name.
     *
     * If neither the first nor last name are defined, the user's email address
     * will be returned.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        $fullName = trim("{$this->first_name} {$this->last_name}");

        return $fullName ?: $this->email;
    }

    /**
     * Gets the user's first (and only) role.
     *
     * The Sentinel package (which we use for handling users) supports multiple
     * roles per user, but our system just needs one (which is handled at the
     * application level), so we'll return it here.
     *
     * @return \Cartalyst\Sentinel\Roles\EloquentRole
     */
    public function getRoleAttribute()
    {
        return $this->roles()->first();
    }
}
