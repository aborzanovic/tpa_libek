<?php

namespace App\Auth\Role;

class Repository
{
    /**
     * Gets the available role options, as defined in the appropriate language
     * file.
     *
     * @return array
     */
    public function getOptions()
    {
        return trans('roles');
    }
}
