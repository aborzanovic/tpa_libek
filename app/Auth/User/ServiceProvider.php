<?php

namespace App\Auth\User;

use App\Auth\User;
use App\Auth\User\Repository as UserRepository;
use Creitive\Module\ServiceProvider as BaseServiceProvider;
use DaveJamesMiller\Breadcrumbs\Manager as BreadcrumbManager;
use DaveJamesMiller\Breadcrumbs\Generator as BreadcrumbGenerator;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Routing\Registrar as RegistrarContract;
use Illuminate\Routing\Router;
use URL;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function boot()
    {
        $this->registerRoutePatterns($this->app['router'], $this->app);
        $this->registerAdminRoutes($this->app['router']);
        $this->registerAdminBreadcrumbs($this->app['breadcrumbs']);
    }

    /**
     * {@inheritDoc}
     */
    public function register()
    {
    }

    /**
     * Registers the route patterns.
     *
     * @param \Illuminate\Routing\Router $router
     * @param \Illuminate\Contracts\Container\Container $container
     * @return void
     */
    protected function registerRoutePatterns(Router $router, Container $container)
    {
        $idRegex = $this->routePatternRegexes['id'];

        $router->pattern('user', $idRegex);

        $router->bind('user', function ($value) use ($container) {
            return $container->make(UserRepository::class)->findOrFail($value);
        });
    }

    /**
     * Registers admin panel routes.
     *
     * @param \Illuminate\Contracts\Routing\Registrar $router
     * @return void
     */
    protected function registerAdminRoutes(RegistrarContract $router)
    {
        $attributes = [
            'prefix' => 'admin/users',
            'middleware' => ['auth', 'permissions'],
            'namespace' => 'App\Auth\Http\Controllers\Admin\User',
        ];

        $router->group($attributes, function (RegistrarContract $router) {
            $router->get('', 'Controller@index');

            $router->get('create', 'Controller@create');
            $router->post('', 'Controller@store');

            $router->get('{user}/edit', 'Controller@edit');
            $router->put('{user}', 'Controller@update');

            $router->get('{user}/delete', 'Controller@confirmDelete');
            $router->delete('{user}', 'Controller@delete');
        });
    }

    /**
     * Registers breadcrumbs for the admin panel.
     *
     * @param \DaveJamesMiller\Breadcrumbs\Manager $breadcrumbs
     * @return void
     */
    protected function registerAdminBreadcrumbs(BreadcrumbManager $breadcrumbs)
    {
        $breadcrumbs->register('admin::users', function (BreadcrumbGenerator $breadcrumbs) {
            $breadcrumbs->parent('admin::home');

            $url = URL::action('App\Auth\Http\Controllers\Admin\User\Controller@index');

            $breadcrumbs->push(trans('admin/navigation.users'), $url);
        });

        $breadcrumbs->register('admin::users.show', function (BreadcrumbGenerator $breadcrumbs, User $user) {
            $breadcrumbs->parent('admin::users');

            $breadcrumbs->push($user->full_name);
        });

        $breadcrumbs->register('admin::users.create', function (BreadcrumbGenerator $breadcrumbs) {
            $breadcrumbs->parent('admin::users');

            $url = URL::action('App\Auth\Http\Controllers\Admin\User\Controller@create');

            $breadcrumbs->push(trans('admin/users.titles.create'), $url);
        });

        $breadcrumbs->register('admin::users.edit', function (BreadcrumbGenerator $breadcrumbs, User $user) {
            $breadcrumbs->parent('admin::users.show', $user);

            $url = URL::action('App\Auth\Http\Controllers\Admin\User\Controller@edit', ['user' => $user->id]);

            $breadcrumbs->push(trans('admin/users.titles.edit'), $url);
        });

        $breadcrumbs->register('admin::users.delete', function (BreadcrumbGenerator $breadcrumbs, User $user) {
            $breadcrumbs->parent('admin::users.show', $user);

            $url = URL::action('App\Auth\Http\Controllers\Admin\User\Controller@confirmDelete', ['user' => $user->id]);

            $breadcrumbs->push(trans('admin/users.titles.delete'), $url);
        });
    }
}
