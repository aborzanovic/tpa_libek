<?php

namespace App\Auth\Login;

use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Sentinel;
use Creitive\Managers\AbstractBaseManager;
use Illuminate\Session\Store as Session;
use Illuminate\Support\MessageBag;
use Illuminate\Translation\Translator;

class Manager extends AbstractBaseManager
{
    /**
     * A Sentinel instance.
     *
     * @var \Cartalyst\Sentinel\Sentinel
     */
    protected $sentinel;

    /**
     * A Translator instance.
     *
     * @var \Illuminate\Translation\Translator
     */
    protected $translator;

    /**
     * A Session store instance.
     *
     * @var \Illuminate\Session\Store
     */
    protected $session;

    public function __construct(Sentinel $sentinel, Translator $translator, Session $session)
    {
        parent::__construct();

        $this->sentinel = $sentinel;
        $this->translator = $translator;
        $this->session = $session;
    }

    /**
     * Attempts to log the user in.
     *
     * Returns `true` on success and `false` on failure.
     *
     * If successful, the user's CSRF token will be regenerated for security
     * purposes.
     *
     * If unsuccessful, the authentication errors will be stored in
     * `$this->errors`.
     *
     * @param array $inputData
     * @return boolean
     */
    public function login(array $inputData)
    {
        $credentials = [
            'email' => $inputData['email'],
            'password' => $inputData['password'],
        ];

        $rememberMe = array_key_exists('remember_me', $inputData);

        try {
            if ($this->sentinel->authenticate($credentials, $rememberMe)) {
                $this->session->regenerateToken();

                return true;
            }
        } catch (NotActivatedException $exception) {
            $this->errors->add('email', $this->translator->get('login.errors.email.notActivated'));

            return false;
        } catch (ThrottlingException $exception) {
            $this->errors->add('email', $this->translator->get('login.errors.email.suspended'));

            return false;
        }

        $this->errors->add('email', $this->translator->get('login.errors.email.unregistered'));

        return false;
    }

    /**
     * Logs the current user out.
     *
     * The user's CSRF token will be regenerated for security purposes.
     *
     * @return boolean
     */
    public function logout()
    {
        $this->sentinel->logout();
        $this->session->regenerateToken();

        return true;
    }
}
