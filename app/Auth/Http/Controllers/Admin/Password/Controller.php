<?php

namespace App\Auth\Http\Controllers\Admin\Password;

use App\Auth\Http\Requests\Admin\Password\UpdateRequest;
use App\Auth\User\Repository as UserRepository;
use App\Http\Controllers\Admin\Controller as BaseController;
use Cartalyst\Sentinel\Sentinel;
use Illuminate\Support\MessageBag;
use URL;

class Controller extends BaseController
{
    public function __construct()
    {
        parent::__construct();

        $this->viewData->bodyDataPage = 'admin-password';
        $this->viewData->pageTitle->setPage(trans('admin/password.module'));
        $this->viewData->navigation->get('admin.main')->setActive('password');
    }

    /**
     * Gets the password update form for the current user.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $data = [
            'submitUrl' => URL::action(static::class.'@update'),
        ];

        return view('admin.password.edit', $data);
    }

    /**
     * Updates the current user's password.
     *
     * @param \App\Auth\Http\Requests\Admin\Password\UpdateRequest $request
     * @param \Cartalyst\Sentinel\Sentinel $sentinel
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, UserRepository $userRepository, Sentinel $sentinel)
    {
        $userRepository->setPassword($sentinel->getUser(), $request->get('new_password'));

        $successMessage = trans('admin/password.successMessages.update');

        return redirect()->action(static::class.'@index')
            ->with('successMessages', new MessageBag([$successMessage]));
    }
}
