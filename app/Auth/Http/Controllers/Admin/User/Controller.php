<?php

namespace App\Auth\Http\Controllers\Admin\User;

use App\Auth\Http\Requests\Admin\User\StoreRequest;
use App\Auth\Http\Requests\Admin\User\UpdateRequest;
use App\Auth\Role\Repository as RoleRepository;
use App\Auth\User;
use App\Auth\User\Repository as UserRepository;
use App\Http\Controllers\Admin\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Redirect;

class Controller extends BaseController
{
    /**
     * A UserRepository instance.
     *
     * @var \App\Auth\User\Repository
     */
    protected $userRepository;

    /**
     * A RoleRepository instance.
     *
     * @var \App\Auth\Role\Repository
     */
    protected $roleRepository;

    public function __construct(UserRepository $userRepository, RoleRepository $roleRepository)
    {
        parent::__construct();

        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;

        $this->viewData->bodyDataPage = 'admin-users';
        $this->viewData->pageTitle->setPage(trans('admin/users.module'));
        $this->viewData->navigation->get('admin.main')->setActive('users');
    }

    /**
     * Shows all users.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->has('roles')) {
            $users = $this->userRepository->getUsersWithAnyRoles($request->get('roles'));
        } else {
            $users = $this->userRepository->getAll();
        }

        $data = [
            'users' => $users,
        ];

        return view('admin.users.index', $data);
    }

    /**
     * Displays the user create form.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $data = [
            'roleOptions' => $this->roleRepository->getOptions(),
            'defaultRoleOption' => 'user',
        ];

        return view('admin.users.create', $data);
    }

    /**
     * Saves a new user.
     *
     * @param \App\Auth\Http\Requests\Admin\User\StoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $user = $this->userRepository->create($request->all());

        $successMessage = trans('admin/users.successMessages.create');

        return Redirect::action(static::class.'@edit', ['user' => $user->id])
            ->with('successMessages', new MessageBag([$successMessage]));
    }

    /**
     * Shows the specified user.
     *
     * @param \App\Auth\User $user
     * @return \Illuminate\View\View
     */
    public function edit(User $user)
    {
        $data = [
            'roleOptions' => $this->roleRepository->getOptions(),
            'user' => $user,
        ];

        return view('admin.users.edit', $data);
    }

    /**
     * Updates the specified user.
     *
     * @param \App\Auth\Http\Requests\Admin\User\UpdateRequest $request
     * @param \App\Auth\User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request, User $user)
    {
        $this->userRepository->update($user, $request->all());

        $successMessage = trans('admin/users.successMessages.edit');

        return Redirect::action(static::class.'@edit', ['user' => $user->id])
            ->with('successMessages', new MessageBag([$successMessage]));
    }

    /**
     * Displays the user deletion confirmation form.
     *
     * @param \App\Auth\User $user
     * @return void
     */
    public function confirmDelete(User $user)
    {
        $data = [
            'user' => $user,
        ];

        return view('admin.users.delete', $data);
    }

    /**
     * Deletes a user.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Auth\User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request, User $user)
    {
        if ($request->get('action') !== 'confirm') {
            return Redirect::action(static::class.'@index');
        }

        $this->userRepository->delete($user);

        $successMessage = trans('admin/users.successMessages.delete', ['fullName' => $user->full_name]);

        return Redirect::action(static::class.'@index')
            ->with('successMessages', new MessageBag([$successMessage]));
    }
}
