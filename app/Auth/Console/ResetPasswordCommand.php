<?php

namespace App\Auth\Console;

use App\Auth\User;
use Cartalyst\Sentinel\Sentinel;
use Illuminate\Console\Command;
use Str;
use Symfony\Component\Console\Input\InputArgument;

class ResetPasswordCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'user:reset-password';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resets a user\'s password';

    /**
     * The Sentinel instance.
     *
     * @var \Cartalyst\Sentinel\Sentinel
     */
    protected $sentinel;

    /**
     * A User model instance.
     *
     * @var \App\Auth\User
     */
    protected $userModel;

    public function __construct(Sentinel $sentinel, User $userModel)
    {
        parent::__construct();

        $this->sentinel = $sentinel;
        $this->userModel = $userModel;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $user = $this->userModel->whereEmail($this->argument('email'))->first();

        if (is_null($user)) {
            $this->error('The user with the specified email does not exist!');

            return 1;
        }

        $password = $this->resetPassword($user, $this->argument('password'));

        $this->info("User's password reset to: [{$password}]");
    }

    /**
     * Resets the user's password to the specified password or a random
     * 32-character string, in one isn't set.
     *
     * Returns the new password.
     *
     * @param \App\Auth\User $user
     * @param string|null $password
     * @return string
     */
    public function resetPassword($user, $password = null)
    {
        $password = $password ?: Str::random(32);

        $credentials = [
            'password' => $password,
        ];

        $this->sentinel->update($user, $credentials);

        return $password;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['email', InputArgument::REQUIRED, 'Email address'],
            ['password', InputArgument::OPTIONAL, 'Password (if one is not provided, a random 32-character string will be generated)'],
        ];
    }
}
