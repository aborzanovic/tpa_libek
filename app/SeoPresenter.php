<?php

namespace App;

use Creitive\Html\DefaultSeoPresenter;

class SeoPresenter extends DefaultSeoPresenter
{
    public function __construct($siteName, $locale, $description, $defaultImage)
    {
        $this->siteName = $siteName;
        $this->type = 'website';
        $this->locale = $locale;
        $this->title = null;
        $this->description = $description;
        $this->image = $defaultImage;
    }
}
