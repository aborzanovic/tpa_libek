<?php

namespace App\AssetManager;

use App\AssetManager\AssetManager;
use Creitive\Asset\Factory as AssetFactory;
use Creitive\ScriptHandler\ScriptHandler;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function register()
    {
        $this->app->singleton(AssetManager::class, function ($app) {
            return new AssetManager(
                $app->environment(),
                $app[AssetFactory::class],
                $app[ScriptHandler::class]
            );
        });
    }
}
