<?php

namespace Creitive\Models;

interface NestableSluggableInterface
{
    /**
     * The `full_slug` attribute getter.
     *
     * @return string
     */
    public function getFullSlugAttribute();

    /**
     * The `slug_path` attribute getter.
     *
     * @return array
     */
    public function getSlugPathAttribute();
}
