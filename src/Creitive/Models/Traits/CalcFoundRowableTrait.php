<?php

namespace Creitive\Models\Traits;

use Creitive\Database\Query\Builder;
use DB;
use Exception;

trait CalcFoundRowableTrait
{
    /**
     * Get a new query builder instance for the connection.
     *
     * This overrides the method from the Eloquent model class, so that we could
     * use our own `sqlCalcFoundRows()` on the query builder.
     *
     * @return \Creitive\Database\Query\Builder
     */
    protected function newBaseQueryBuilder()
    {
        $conn = $this->getConnection();

        $grammar = $conn->getQueryGrammar();

        return new Builder($conn, $grammar, $conn->getPostProcessor());
    }

    /**
     * Gets the last total row count calculated by MySql, when used with the
     * `SQL_CALC_FOUND_ROWS` option - this option must be set on the query
     * builder in order for this method to work.
     *
     * @return integer
     */
    public static function getLastTotalRowCount()
    {
        $results = DB::select(
            'SELECT FOUND_ROWS() AS `total`;'
        );

        if (count($results) !== 1) {
            throw new Exception('getLastTotalRowCount() got a weird result from the database. Please check the logs for more info.');
        }

        $result = reset($results);

        return (int) $result->total;
    }
}
