<?php

namespace Creitive\Models\Traits;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

trait PublishableTrait
{
    /**
     * Limits the results to those that have been published.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished(Builder $query)
    {
        $now = Carbon::now();

        return $query->whereNotNull('published_at')->where('published_at', '<=', $now);
    }

    /**
     * Checks whether this instance has been published.
     *
     * @return boolean
     */
    public function isPublished()
    {
        return (!is_null($this->published_at) && !$this->published_at->isFuture());
    }
}
