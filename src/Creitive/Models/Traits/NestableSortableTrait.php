<?php

namespace Creitive\Models\Traits;

use Creitive\Models\Traits\BaseSortableTrait;
use DB;
use Exception;
use Log;

trait NestableSortableTrait
{
    use BaseSortableTrait;

    /**
     * Boots this trait.
     *
     * @return void
     */
    public static function bootNestableSortableTrait()
    {
        static::bootBaseSortableTrait();
    }

    /**
     * Updates the sort order.
     *
     * Accepts an ordered array of IDs, which represents the new order.
     *
     * @param array $newSortOrder
     * @return boolean
     */
    public function updateSortOrder(array $newSortOrder = [])
    {
        if (!is_array($newSortOrder)) {
            return false;
        }

        if (empty($newSortOrder)) {
            return true;
        }

        $table = $this->getTable();

        try {
            $sortWhenThen = [];
            $parentWhenThen = [];
            $ids = [];

            $sort = 0;

            foreach ($newSortOrder as $item) {
                ++$sort;
                $id = (int) $item['id'];
                $parentId = ((int) $item['parentId']) ?: 'NULL';

                $ids[] = $id;
                $sortWhenThen[] = "WHEN {$id} THEN {$sort}";
                $parentWhenThen[] = "WHEN {$id} THEN {$parentId}";
            }

            $sortWhenThen = implode(' ', $sortWhenThen);
            $parentWhenThen = implode(' ', $parentWhenThen);
            $ids = implode(',', $ids);

            DB::update(
                "UPDATE `{$table}`
                SET
                    `sort` =
                        CASE `id`
                            {$sortWhenThen}
                        END,
                    `parent_id` =
                        CASE `id`
                            {$parentWhenThen}
                        END
                WHERE `id` IN ({$ids});"
            );

            return true;
        } catch (Exception $e) {
            Log::error($e);

            return false;
        }
    }
}
