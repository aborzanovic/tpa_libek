<?php

namespace Creitive\Models\Traits;

use Creitive\Models\ChildImageableInterface;
use Creitive\Models\GeoableInterface;
use Exception;
use ImageUploader;
use ImageUrlGenerator;

trait ImageableTrait
{
    /**
     * Gets the configuration for this model's images.
     *
     * This should be overriden in the appropriate model where this trait is
     * used, and it should return an array with the image configuration.
     *
     * In general, this array should contain a 'versions' key, with the
     * appropriate version configuration listed within.
     *
     * Sorry for the lack of documentation on how this works - no time to
     * explain right now. To be honest? We're on a deadline, and it's likely we
     * won't ever find the time to document this here, so to see how it's
     * supposed to work, check some of the models that use this, and try to
     * figure it out. Good luck!
     *
     * @todo Document how this works.
     * @return array
     */
    abstract public function getImageConfiguration();

    /**
     * Gets an image of a specified version and variant, or, if not found, a
     * default image - the convention is to put the default images in the public
     * folder, under the path `images/default/ClassName/version/variant.png`.
     *
     * @param string $version
     * @param string $variant
     * @param boolean $checkChildren
     * @return string
     */
    public function getImage($version, $variant, $checkChildren = true)
    {
        $imageVersionVariant = "image_{$version}_{$variant}";

        if (isset($this->$imageVersionVariant) && $this->$imageVersionVariant) {
            return ImageUrlGenerator::generate($this->{$imageVersionVariant});
        }

        if ($this->canHaveChildImages() && $checkChildren) {
            return $this->getChildImage($version, $variant);
        }

        return $this->getDefaultImage($version, $variant);
    }

    /**
     * Checks whether the model has an image of the specified version and
     * variant.
     *
     * This checks not only whether it is configured, but also whether there is
     * an uploaded image. It also checks child images, unless the third argument
     * is set to `false`;
     *
     * @param string $version
     * @param string $variant
     * @param boolean $checkChildren
     * @return boolean
     */
    public function hasImage($version, $variant, $checkChildren = true)
    {
        $imageVersionVariant = "image_{$version}_{$variant}";

        if (isset($this->$imageVersionVariant) && $this->$imageVersionVariant) {
            return true;
        }

        return $checkChildren ? $this->hasChildImage($version, $variant) : false;
    }

    /**
     * Checks whether the model has a child image of the specified version and
     * variant.
     *
     * @param string $version
     * @param string $variant
     * @return boolean
     */
    public function hasChildImage($version, $variant)
    {
        if (!$this->canHaveChildImages()) {
            return false;
        }

        if ($this->getChildImages()->isEmpty()) {
            return false;
        }

        $firstImage = $this->getChildImages()->first();

        return $firstImage ? $firstImage->hasImage($version, $variant) : false;
    }

    /**
     * Gets the default image for the requested version and variant.
     *
     * @param string $version
     * @param string $variant
     * @return string
     */
    public function getDefaultImage($version, $variant)
    {
        $prefix = $this->getImagePrefix();

        if ($this->language) {
            $language = ".{$this->language}";
        } else {
            $language = '';
        }

        return "/images/default/{$prefix}/{$version}/{$variant}{$language}.png";
    }

    /**
     * Gets the first image of the specified version and variant from the
     * related images of the model.
     *
     * @param string $version
     * @param string $variant
     * @return string
     */
    public function getChildImage($version, $variant)
    {
        if (!$this->canHaveChildImages()) {
            return false;
        }

        /*
         * First we'll try to fetch the related images and return the first one.
         */
        $firstImage = $this->getChildImages()->first();

        if ($firstImage) {
            return $firstImage->getImage($version, $variant);
        }

        /*
         * If that didn't work, we have to find the model of the related images,
         * and return its default image of the specified version and variant.
         */
        $imagesRelation = $this->getChildImagesRelation();

        $imagesModel = $imagesRelation->getRelated();

        return $imagesModel->getDefaultImage($version, $variant);
    }

    /**
     * Gets the child images for this model instance.
     *
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function getChildImages()
    {
        if (!$this->canHaveChildImages()) {
            throw new Exception(get_called_class().' cannot have child images!');
        }

        $childImagesAttributeName = $this->getChildImagesAttributeName();

        return $this->{$childImagesAttributeName};
    }

    /**
     * Gets the child images relation for this model instance.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getChildImagesRelation()
    {
        if (!$this->canHaveChildImages()) {
            throw new Exception(get_called_class().' cannot have child images!');
        }

        $childImagesAttributeName = $this->getChildImagesAttributeName();

        return $this->{$childImagesAttributeName}();
    }

    /**
     * Checks whether this model has child images, by testing whether the
     * appropriate interface is implemented.
     *
     * @return boolean
     */
    public function canHaveChildImages()
    {
        return $this instanceof ChildImageableInterface;
    }

    /**
     * Uploads an image, and sets the relevant properties on the instance.
     *
     * @param mixed $file
     * @param string $version
     * @return boolean
     */
    public function uploadImage($file, $version)
    {
        if (!$this->imageVersionConfigured($version)) {
            throw new Exception('Image version '.$version.' not configured in model: '.get_called_class());
        }

        $prefix = $this->getImagePrefix();
        $versionConfiguration = $this->getImageVersionConfiguration($version);

        $data = ImageUploader::upload($file, $version, $versionConfiguration, $prefix);

        if (!$data) {
            return false;
        }

        foreach ($versionConfiguration as $variant => $params) {
            $imageProperty = "image_{$version}_{$variant}";
            $this->{$imageProperty} = $data[$variant];
        }

        /*
         * Handle geo-coordinates taken from the original image, if available.
         *
         * There are prettier ways to solve this, but meh.
         */

        if ($this instanceof GeoableInterface
            && array_key_exists('exif', $data)
            && !is_null($data['exif'])
            && $gps = $data['exif']->getGPS()
        ) {
            list($this->latitude, $this->longitude) = explode(',', $gps);
        }

        return true;
    }

    /**
     * Deletes this model's images of a specified version.
     *
     * Doesn't actually delete the images on the filesystem, just unlinks them
     * within the database.
     *
     * @param string $version
     * @return void
     */
    public function deleteImage($version)
    {
        if (!$this->imageVersionConfigured($version)) {
            throw new Exception('Image version '.$version.' not configured in model: '.get_called_class());
        }

        $versionConfiguration = $this->getImageVersionConfiguration($version);

        foreach ($imageVersions as $version => $params) {
            $imageProperty = "image_{$version}";
            $this->{$imageProperty} = null;
        }
    }

    /**
     * Gets the configured image prefix.
     *
     * @return string
     */
    public function getImagePrefix()
    {
        $classTree = explode('\\', get_called_class());

        return end($classTree);
    }

    /**
     * Gets the image configuration for the requested version.
     *
     * @param string $version
     * @return array
     */
    public function getImageVersionConfiguration($version)
    {
        $imageConfiguration = $this->getImageConfiguration();

        return $imageConfiguration['versions'][$version];
    }

    /**
     * Checks whether this model contains configuration for the passed image
     * version.
     *
     * @param string $version
     * @return boolean
     */
    public function imageVersionConfigured($version)
    {
        $imageConfiguration = $this->getImageConfiguration();

        return isset($imageConfiguration['versions'][$version]);
    }

    /**
     * Checks whether a version and variant of an image have been configured for
     * this model.
     *
     * @param string $version
     * @param string $variant
     * @return boolean
     */
    public function imageVersionVariantConfigured($version, $variant)
    {
        $imageConfiguration = $this->getImageConfiguration();

        return (
            (isset($imageConfiguration['versions'][$version]))
            && (isset($imageConfiguration['versions'][$version][$variant]))
        );
    }
}
