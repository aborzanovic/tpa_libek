<?php

namespace Creitive\Models\Traits;

use Creitive\Models\Traits\BaseSortableTrait;
use DB;
use Exception;
use Log;

trait SortableTrait
{
    use BaseSortableTrait;

    /**
     * Boots this trait.
     *
     * @return void
     */
    public static function bootSortableTrait()
    {
        static::bootBaseSortableTrait();
    }

    /**
     * Updates the sort order.
     *
     * Accepts an ordered array of IDs, which represents the new order.
     *
     * @param array $newSortOrder
     * @return boolean
     */
    public function updateSortOrder(array $newSortOrder = [])
    {
        if (!is_array($newSortOrder)) {
            return false;
        }

        if (empty($newSortOrder)) {
            return true;
        }

        $table = $this->getTable();

        try {
            $whenThen = [];
            $ids = [];

            $sort = 0;

            foreach ($newSortOrder as $id) {
                $id = (int) $id;
                ++$sort;

                $ids[] = $id;
                $whenThen[] = "WHEN {$id} THEN {$sort}";
            }

            $whenThen = implode(' ', $whenThen);
            $ids = implode(',', $ids);

            DB::update(
                "UPDATE `{$table}`
                SET `sort` = CASE `id`
                    {$whenThen}
                END
                WHERE `id` IN ({$ids});"
            );

            return true;
        } catch (Exception $e) {
            Log::error($e);

            return false;
        }
    }
}
