<?php

namespace Creitive\Models\Traits;

use DB;
use Illuminate\Database\Eloquent\Builder;

trait GeoableTrait
{
    /**
     * Limits the results to those that are within `$maxDistance` (which should
     * be an integer in `meters`) from the passed coordinates.
     *
     * Requires the database table to have `latitude` and `longitude` columns,
     * and no `distance` column.
     *
     * Due to the way how this method modifies the query, the calling code must
     * explicitly perform `select('*')` on the query builder first; otherwise,
     * the other columns won't be fetched at all.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $latitude
     * @param string $longitude
     * @param integer $maxDistance
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNear(Builder $query, $latitude, $longitude, $maxDistance)
    {
        $latitude = DB::connection()->getPdo()->quote($latitude);
        $longitude = DB::connection()->getPdo()->quote($longitude);

        /*
         * Unfortunately, we cannot use bound parameters here, because they are
         * not supported by the query builder for this use case. We cannot use
         * `DB::select()`, as we need this to be a chainable Eloquent Builder
         * method, hence we must rely on its functionality.
         */

        $haversine = "
        (
            6371 * ACOS(
                COS(RADIANS({$latitude})) * COS(RADIANS(`latitude`)) * COS(RADIANS(`longitude`) - RADIANS({$longitude}))
                + SIN(RADIANS({$latitude})) * SIN(RADIANS(`latitude`))
            )
        ) AS `distance`";

        return $query
            ->withCoordinates()
            ->addSelect(DB::raw($haversine))
            ->having('distance', '<=', $maxDistance/1000)
        ;
    }

    /**
     * Limits the results to those that have defined coordinates.
     *
     * An item is considered to have defined coordinates if neither of its
     * `latitude` and `longitude` columns are `NULL`.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithCoordinates(Builder $query)
    {
        return $query->whereNotNull('latitude')->whereNotNull('longitude');
    }

    /**
     * Checks whether the current instance has coordinates.
     *
     * @return boolean
     */
    public function hasCoordinates()
    {
        return (!is_null($this->latitude)) && (!is_null($this->longitude));
    }
}
