<?php

namespace Creitive\Models\Traits;

use DateTime;
use Illuminate\Database\Eloquent\Builder;

trait DateableTrait
{
    /**
     * Limits the results to those with a specific date (ignoring the time).
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $column
     * @param \DateTime $date
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereDate(Builder $query, $column, DateTime $date)
    {
        $dateLike = $date->format('Y-m-d').' %';

        return $query->where($column, 'LIKE', $dateLike);
    }
}
