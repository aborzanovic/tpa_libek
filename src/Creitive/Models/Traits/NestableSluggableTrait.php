<?php

namespace Creitive\Models\Traits;

use LogicException;

trait NestableSluggableTrait
{
    /**
     * Must return a string which is the name of an Eloquent relationship
     * defined on the model, that resolves the "parent" `BelongsTo`
     * relationship.
     *
     * @return string
     */
    abstract protected function getNestableSluggableParentAttribute();

    /**
     * {@inheritDoc}
     */
    public function getFullSlugAttribute()
    {
        return implode('/', $this->slug_path);
    }

    /**
     * {@inheritDoc}
     */
    public function getSlugPathAttribute()
    {
        $parentAttribute = $this->getNestableSluggableParentAttribute();

        $parent = $this->{$parentAttribute};

        if (is_null($parent)) {
            return [$this->slug];
        }

        $path = $parent->slug_path;

        array_push($path, $this->slug);

        return $path;
    }
}
