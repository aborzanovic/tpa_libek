<?php

namespace Creitive\Models\Traits;

trait ChildImageableTrait
{
    /**
     * {@inheritDoc}
     */
    public function getChildImagesAttributeName()
    {
        return 'images';
    }
}
