<?php

namespace Creitive\Models\Traits;

use Creitive\Models\NestableInterface;

trait NestableTrait
{
    /**
     * {@inheritDoc}
     */
    public function setParent(NestableInterface $item = null)
    {
        $this->setRelation('parent', $item);
    }

    /**
     * {@inheritDoc}
     */
    public function addChild(NestableInterface $item)
    {
        $this->children->push($item);
    }

    /**
     * {@inheritDoc}
     */
    public function parent()
    {
        return $this->belongsTo(get_called_class(), 'parent_id');
    }

    /**
     * {@inheritDoc}
     */
    public function children()
    {
        return $this->hasMany(get_called_class(), 'parent_id');
    }

    /**
     * {@inheritDoc}
     */
    public function hasParent()
    {
        return $this->parent_id !== null;
    }

    /**
     * {@inheritDoc}
     */
    public function hasChildren()
    {
        return !$this->children->isEmpty();
    }

    /**
     * {@inheritDoc}
     */
    public function root()
    {
        if (!$this->hasParent()) {
            return $this;
        }

        return $this->parent->root();
    }

    /**
     * Initializes this item's children.
     *
     * @return void
     */
    public function initializeChildren()
    {
        $this->setRelation('children', $this->newCollection());
    }
}
