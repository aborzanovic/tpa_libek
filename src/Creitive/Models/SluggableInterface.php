<?php

namespace Creitive\Models;

interface SluggableInterface
{
    /**
     * Automatically performs the sluggification.
     *
     * Returns `true` on success, or `false` on failure.
     *
     * @return boolean
     */
    public function sluggify();

    /**
     * Validates whether the passed string is a valid slug.
     *
     * @param string $string
     * @return boolean
     */
    public function validateSlug($slug);
}
