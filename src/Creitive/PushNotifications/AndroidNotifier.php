<?php

namespace Creitive\PushNotifications;

use Sly\NotificationPusher\PushManager;
use Sly\NotificationPusher\Adapter\Gcm as GcmAdapter;
use Sly\NotificationPusher\Collection\DeviceCollection;
use Sly\NotificationPusher\Model\Device;
use Sly\NotificationPusher\Model\Message;
use Sly\NotificationPusher\Model\Push;

class AndroidNotifier
{
    protected $apiKey = '';

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * Sends push notifications to android users
     *
     * @param array $tokens
     * @param string $message
     * @return void
     */
    public function send(array $tokens, $message)
    {
        $pushManager = new PushManager(PushManager::ENVIRONMENT_DEV);

        $gcmAdapter = new GcmAdapter(array(
            'apiKey' => $this->apiKey,
        ));

        $devices = $this->generateDevices($tokens);

        $message = new Message($message);

        $push = new Push($gcmAdapter, $devices, $message);
        $pushManager->add($push);
        $response = $pushManager->push();

        return $this->getUnregisterendDeviceTokens($response);
    }

    /**
     * Generates collection of devices from array of tokens
     *
     * @param array $tokens
     * @return \Sly\NotificationPusher\Collection\DeviceCollection
     */
    protected function generateDevices(array $tokens)
    {
        $devicesArray = [];

        foreach ($tokens as $token) {
            $devicesArray[] = new Device($token);
        }

        return new DeviceCollection($devicesArray);
    }

    /**
     * Removes unregistered device tokens from database
     *
     * @param Sly\NotificationPusher\PushManager $response
     * @return void
     */
    protected function getUnregisterendDeviceTokens(PushManager $response)
    {
        $badTokens = [];

        foreach ($response as $result) {
            $result = $result->getAdapter()->getResponse()->getResults();

            foreach ($result as $token => $status) {
                if (! is_array($status) || isset($status['error'])) {
                    $badTokens[] = $token;
                }
            }
        }

        return $badTokens;
    }
}
