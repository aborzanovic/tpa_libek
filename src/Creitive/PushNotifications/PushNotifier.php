<?php

namespace Creitive\PushNotifications;

use App\Devices\Device;
use App\Devices\Device\Repository as DeviceRepository;
use Creitive\PushNotifications\AndroidNotifier;
use Creitive\PushNotifications\IosNotifier;

class PushNotifier
{
    /**
     * Insance of IosNotifier
     *
     * @var \Creitive\PushNotifier\IosNotifier
     */
    protected $iosNotifier;

    /**
     * Insance of AndroidNotifier
     *
     * @var \Creitive\PushNotifier\AndroidNotifier
     */
    protected $androidNotifier;

    /**
     * A Device Repository.
     *
     * @var \App\Devices\Device\Repository
     */
    protected $deviceRepository;

    public function __construct(AndroidNotifier $androidNotifier, IosNotifier $iosNotifier, DeviceRepository $deviceRepository)
    {
        $this->androidNotifier = $androidNotifier;
        $this->iosNotifier = $iosNotifier;
        $this->deviceRepository = $deviceRepository;
    }

    /**
     * Notifies a group of user about or all users if nothing is passed trough
     * $users variable
     *
     * @param string $message
     * @param array|null $options
     * @param \Illuminate\Support\Collection|null $users
     * @return [type]
     */
    public function notify($message, array $options = null, Collection $users = null)
    {
        $devices = $this->getDevices($users);

        $iosDevices = $devices->where('device_type', Device::TYPE_IOS);
        $androidDevices = $devices->where('device_type', Device::TYPE_ANDROID);

        if (!$iosDevices->isEmpty()) {
            $this->sendIosNotifications($message, $options, $iosDevices);
        }

        if (!$androidDevices->isEmpty()) {
            $this->sendAndroidNotifications($message, $options, $androidDevices);
        }
    }

    /**
     * Gets devices for notifing for list users
     *
     * @param \Illuminate\Support\Collection|null $users
     * @return \Eloquent\Database\Eloquent\Collection
     */
    protected function getDevices(Collection $users = null)
    {
        if ($users === null || $users->isEmpty()) {
            return $this->deviceRepository->getAll();
        }

        return $this->deviceRepository->getByUserIds($users->modelKeys());
    }

    /**
     * Sends notifications to ios devices using right adapter
     *
     *  @param string $message
     * @param array|null $options
     * @param \Illuminate\Support\Collection|null $devices
     */
    protected function sendIosNotifications($message, $options, $devices)
    {
        $deviceTokens = $devices->lists('device_token')->toArray();

        $notificationMessage = [];
        $notificationMessage->push($message);

        if ($options) {
            $notificationMessage->push($options);
        }

        $notifiedDeviceTokens = $this->iosNotifier->send($deviceTokens, $notificationMessage);
        $badDeviceTokens = array_diff($deviceTokens, array_keys($notifiedDeviceTokens));

        $this->removeBadTokens($badDeviceTokens, Device::TYPE_IOS);
    }

    /**
     * Sends notifications to android devices using right adapter
     *
     *  @param string $message
     * @param array|null $options
     * @param \Illuminate\Support\Collection|null $devices
     */
    protected function sendAndroidNotifications($message, $options, $devices)
    {


        $notificationMessage = [];
        $notificationMessage['title'] = $message;

        if ($options) {
            $notificationMessage['options'] = $options;
        }

        $badDeviceTokens = $this->androidNotifier->send($devices->lists('device_token')->toArray(), $notificationMessage);
        $this->removeBadTokens($badDeviceTokens, Device::TYPE_ANDROID);
    }

    /**
     * Removes bad device tokens based on server responsers
     *
     * @param array $badTokens
     * @param string $deviceType
     * @return void
     */
    protected function removeBadTokens(array $badTokens, $device_type)
    {
        if (!empty($badTokens)) {
            $this->deviceRepository->removeByTokenIn($badTokens, $device_type);
        }
    }
}
