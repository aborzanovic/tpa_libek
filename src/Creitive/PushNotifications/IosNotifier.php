<?php

namespace Creitive\PushNotifications;

use Sly\NotificationPusher\PushManager;
use Sly\NotificationPusher\Adapter\Apns as ApnsAdapter;
use Sly\NotificationPusher\Collection\DeviceCollection;
use Sly\NotificationPusher\Model\Device;
use Sly\NotificationPusher\Model\Message;
use Sly\NotificationPusher\Model\Push;

class IosNotifier
{
    protected $cerfiticatePath = '';
    protected $cerfiticatePassphrase = '';

    public function __construct($cerfiticatePath, $cerfiticatePassphrase)
    {
        $this->cerfiticatePath = $cerfiticatePath;
        $this->cerfiticatePassphrase = $cerfiticatePassphrase;
    }

    /**
     * Sends push notifications to ios users
     *
     * @param array $tokens
     * @param string $message
     * @return void
     */
    public function send(array $tokens, $message)
    {
        $pushManager = new PushManager(PushManager::ENVIRONMENT_DEV);

        $authParameters = [];
        $authParameters['certificate'] = $this->cerfiticatePath;

        if ($this->cerfiticatePassphrase) {
            $authParameters['passPhrase'] = $this->cerfiticatePassphrase;
        }

        $apnsAdapter = new ApnsAdapter($authParameters);

        $devices = $this->generateDevices($tokens);
        $message = new Message($message['title'], ['custom' => $message['options']]);

        $push = new Push($apnsAdapter, $devices, $message);

        $pushManager->add($push);
        $pushManager->push();

        $feedback = $pushManager->getFeedback($apnsAdapter);
        return array_keys($feedback);
    }

    /**
     * Generates collection of devices from array of tokens
     *
     * @param array $tokens
     * @return \Sly\NotificationPusher\Collection\DeviceCollection
     */
    protected function generateDevices(array $tokens)
    {
        $devicesArray = [];

        foreach ($tokens as $token) {
            $devicesArray[] = new Device($token);
        }

        return new DeviceCollection($devicesArray);
    }
}
