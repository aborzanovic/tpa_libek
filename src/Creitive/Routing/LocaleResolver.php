<?php

namespace Creitive\Routing;

use Exception;

class LocaleResolver
{
    /**
     * An array of available application locales.
     *
     * @var array
     */
    protected $availableLocales;

    /**
     * The current locale.
     *
     * @var string
     */
    protected $locale;

    public function __construct(array $availableLocales, $locale)
    {
        if (!$this->validLocaleConfiguration($availableLocales, $locale)) {
            throw new Exception('Invalid locale configuration - the default locale is missing from the available locales array.');
        }

        $this->availableLocales = $availableLocales;
        $this->locale = $locale;
    }

    /**
     * Validates the locale configuration.
     *
     * Basically, the passed locale's definition must exist in the passed array
     * of available locales.
     *
     * @param array $availableLocales
     * @param string $locale
     * @return boolean
     */
    public function validLocaleConfiguration(array $availableLocales, $locale)
    {
        foreach ($availableLocales as $availableLocale) {
            if ($availableLocale['locale'] === $locale) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns the available locales.
     *
     * @return array
     */
    public function getAvailableLocales()
    {
        return $this->availableLocales;
    }

    /**
     * Gets the locale.
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Sets the locale.
     *
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $properties = $this->getLocaleProperties($locale);

        if (is_null($properties)) {
            throw new Exception('The specified locale doesn\'t exist in the available locales: '.$locale);
        }

        $this->locale = $locale;
    }

    /**
     * Gets the current application language (note that this is not the same as
     * the current locale, as the language might be "en" whereas the locale is
     * "en_US").
     *
     * @return string
     */
    public function getLanguage()
    {
        $properties = $this->getLocaleProperties($this->locale);

        return $properties['language'];
    }

    /**
     * Checks whether the current app is multilingual.
     *
     * @return boolean
     */
    public function isMultilingual()
    {
        return (count($this->availableLocales) > 1);
    }

    /**
     * Returns the appropriate locale according to the passed language.
     *
     * Returns `null` if the specified language isn't found.
     *
     * @param string $language
     * @return string
     */
    public function getLocaleByLanguage($language)
    {
        $properties = $this->getLanguageProperties($language);

        if (!is_null($properties)) {
            return $properties['locale'];
        } else {
            return null;
        }
    }

    /**
     * Gets the name of a language.
     *
     * Returns `null` if the language isn't found.
     *
     * @param string $language
     * @return string|null
     */
    public function getLanguageName($language)
    {
        $properties = $this->getLanguageProperties($language);

        if (!is_null($properties)) {
            return $properties['name'];
        }

        return null;
    }

    /**
     * Checks whether the language exists.
     *
     * @param string $language
     * @return boolean
     */
    public function languageExists($language)
    {
        foreach ($this->availableLocales as $properties) {
            if ($properties['language'] === $language) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks whether the locale exists.
     *
     * @param string $locale
     * @return boolean
     */
    public function localeExists($locale)
    {
        foreach ($this->availableLocales as $properties) {
            if ($properties['locale'] === $locale) {
                return true;
            }
        }

        return false;
    }

    /**
     * Gets the properties array for a locale.
     *
     * Returns `null` if the locale isn't found.
     *
     * @param string $locale
     * @return array|null
     */
    public function getLocaleProperties($locale)
    {
        foreach ($this->availableLocales as $properties) {
            if ($properties['locale'] === $locale) {
                return $properties;
            }
        }

        return null;
    }

    /**
     * Gets the properties array for a language.
     *
     * Returns `null` if the language isn't found.
     *
     * @param string $language
     * @return array|null
     */
    public function getLanguageProperties($language)
    {
        foreach ($this->availableLocales as $properties) {
            if ($properties['language'] === $language) {
                return $properties;
            }
        }

        return null;
    }
}
