<?php

namespace Creitive\Routing\Facades;

use Creitive\Routing\LocaleResolver as BaseLocaleResolver;
use Illuminate\Support\Facades\Facade;

class LocaleResolver extends Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return BaseLocaleResolver::class;
    }
}
