<?php

namespace Creitive\Support;

use Creitive\Support\Serbian;
use DateTime;
use Illuminate\Support\Str as IlluminateStr;

class Str extends IlluminateStr
{
    /**
     * Sluggifies the passed string.
     *
     * @param string $text
     * @param string $separator
     * @return string
     */
    public static function slug($text, $separator = '-')
    {
        $text = Serbian::cyrillicToLatin($text);
        $text = Serbian::latinToAscii($text);

        return parent::slug($text, $separator);
    }

    /**
     * Removes duplicate characters from a string, using native PHP functions.
     *
     * Not terribly efficient, but does the job.
     *
     * @param string $string
     * @param string $char
     * @return string
     */
    public static function removeDuplicates($string, $char = ' ')
    {
        $duplicate = $char.$char;

        while (strpos($string, $duplicate) !== false) {
            $string = str_replace($duplicate, $char, $string);
        }

        return $string;
    }

    /**
     * Creates an excerpt from a long string/text.
     *
     * @param string $string
     * @param int $length
     * @param string $ending
     * @param string $encoding
     * @return string
     */
    public static function createExcerpt($string, $length = 80, $ending = '...', $encoding = 'UTF-8')
    {
        /*
         * If the original string is shorter than the target excerpt length,
         * just return it.
         */
        if (mb_strlen($string, $encoding) <= $length) {
            return $string;
        }

        /*
         * Shorten the string to the desired length.
         */
        $string = mb_substr($string, 0, $length - mb_strlen($ending, $encoding), $encoding);

        /*
         * Find the last space character in the string.
         */
        $lastSpace = mb_strrpos($string, ' ', 0, $encoding);

        /*
         * If no space character is found, return the string concatenated with
         * the ending.
         */
        if ($lastSpace === false) {
            return $string.$ending;
        }

        /*
         * Otherwise, return the string up to the last space, concatenated with
         * the ending.
         */
        return mb_substr($string, 0, $lastSpace, $encoding).$ending;
    }
}
