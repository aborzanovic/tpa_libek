<?php

namespace Creitive\Managers;

use Illuminate\Support\MessageBag;
use Validator;

abstract class AbstractBaseManager
{
    /**
     * Validation rules.
     *
     * @var array
     */
    protected $validationRules = [];

    /**
     * A bag of error messages.
     *
     * @var \Illuminate\Support\MessageBag
     */
    protected $errors;

    public function __construct()
    {
        $this->errors = new MessageBag;
    }

    /**
     * Returns the error message bag.
     *
     * @return \Illuminate\Support\MessageBag
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Gets a validator for a specific ruleset.
     *
     * @param string $ruleset
     * @param array $inputData
     * @param array $messages
     * @return \Illuminate\Validation\Validator
     */
    public function getValidator($ruleset, array $inputData, array $messages = [])
    {
        return Validator::make($inputData, $this->validationRules[$ruleset], $messages);
    }
}
