<?php

namespace Creitive\Commerce;

use DomainException;
use InvalidArgumentException;

class PriceFormatter
{
    /**
     * How many decimals are stored in the database.
     *
     * Since the prices are stored in the database as integers, this controls
     * how many decimal places are stored in the database. For example, if this
     * value is 3, and our price is $1.2345, the database will store the integer
     * `1234`. This means that we need to divide the database price by
     * `10 ^ $storedDecimals` to get the correct value in dollars.
     *
     * @var integer
     */
    protected $storedDecimals = 2;

    /**
     * This is the factor by which the prices are multipled and divided when
     * converting to the different formats.
     *
     * @var integer
     */
    protected $factor = 100;

    /**
     * The decimal separator used in the string representations of the price.
     *
     * @var string
     */
    protected $decimalSeparator = '.';

    /**
     * The thousands separator used in the string representations of the price.
     *
     * @var string
     */
    protected $thousandsSeparator = ',';

    /**
     * Creates a new price formatter instance.
     *
     * The provided argument must be an integer greater than or equal to 0.
     *
     * @param integer $storedDecimals
     * @param string $decimalSeparator
     * @param string $thousandsSeparator
     */
    public function __construct($storedDecimals = 2, $decimalSeparator = '.', $thousandsSeparator = ',')
    {
        $this->setStoredDecimals($storedDecimals);

        $this->decimalSeparator = $decimalSeparator;
        $this->thousandsSeparator = $thousandsSeparator;
    }

    /**
     * Sets how many decimals are stored in the database.
     *
     * @param integer $storedDecimals
     * @return void
     */
    public function setStoredDecimals($storedDecimals)
    {
        if (!is_int($storedDecimals)) {
            throw new InvalidArgumentException('Creitive\\Commerce\\PriceFormatter: stored decimals must be an integer.');
        }

        if ($storedDecimals < 0) {
            throw new DomainException('Creitive\\Commerce\\PriceFormatter: stored decimals must be an integer greater than or equal to 0.');
        }

        $this->storedDecimals = $storedDecimals;
        $this->factor = pow(10, $this->storedDecimals);
    }

    /**
     * Converts a string representation of a price to the database format, based
     * on how many decimal places are stored in the database, and how many
     * should be parsed from the input string.
     *
     * @param string $string
     * @param integer|null $decimalPlaces
     * @param string|null $decimalSeparator
     * @param string|null $thousandsSeparator
     * @return integer
     */
    public function toDatabase($string, $decimalPlaces = null, $decimalSeparator = null, $thousandsSeparator = null)
    {
        if (is_null($decimalPlaces)) {
            $decimalPlaces = $this->storedDecimals;
        }

        if (is_null($decimalSeparator)) {
            $decimalSeparator = $this->decimalSeparator;
        }

        if (is_null($thousandsSeparator)) {
            $thousandsSeparator = $this->thousandsSeparator;
        }

        if (!$this->isValidPrice($string, null, false, $decimalSeparator, $thousandsSeparator)) {
            return false;
        }

        if (empty($string)) {
            return 0;
        }

        $string = str_replace($thousandsSeparator, '', $string);

        if ($this->containsDecimalSeparator($string, $decimalSeparator)) {
            list($wholePart, $decimalPart) = explode($decimalSeparator, $string);

            $decimalPart = substr($decimalPart, 0, $decimalPlaces);

            $string = $wholePart.str_pad($decimalPart, $decimalPlaces, '0', STR_PAD_RIGHT);
        } else {
            $string .= str_repeat('0', $decimalPlaces);
        }

        return (int) $string;
    }

    /**
     * Formats a price as a string with decimals (the number of which is
     * configurable with an optional second argument), in `units/factor`, where
     * `factor` represents how the data is stored in the database.
     *
     * E.g. assuming the decimal separator used is ".", and the thousands
     * separator is ".", this method will convert 123456 (cents) to 1,234.56
     * (dollars), if called like:
     *
     *     $priceFormatter->toString(123456, 2);
     *
     * @param integer $price
     * @param integer $decimalPlaces
     * @return string
     */
    public function toString($price = 0, $decimalPlaces = 2)
    {
        return $this->toFormattedString($price, $decimalPlaces, $this->decimalSeparator, $this->thousandsSeparator);
    }

    /**
     * Formats the price to a PayPal-compatible string, which has a strict
     * predefined format - two decimal places, a "." for the decimal separator,
     * and no thousands separators.
     *
     * We don't need any localization here because PayPal's format is the way it
     * is, regardless of our app.
     *
     * @param integer $price
     * @return string
     */
    public function toPayPalString($price = 0)
    {
        return $this->toFormattedString($price, 2, '.', '');
    }

    /**
     * Converts a price from a PayPal string to the database representation.
     *
     * @param string $price
     * @param integer|null $decimalPlaces
     * @return integer
     */
    public function fromPayPalString($price, $decimalPlaces = null)
    {
        return $this->toDatabase($price, $decimalPlaces, '.', ',');
    }

    /**
     * Rounds a float (stored as an integer*factor), to a specified precision.
     *
     * For example, if we are storing four decimals:
     *
     *     roundDatabasePrice(12345, 0) === 10000
     *     roundDatabasePrice(12345, 2) === 12300
     *     roundDatabasePrice(12355, 2) === 12400
     *
     * @param integer $price
     * @param integer $precision
     * @return integer
     */
    public function roundDatabasePrice($price, $precision = 0)
    {
        $price = round($price / $this->factor, $precision);

        return (int) ($price * $this->factor);
    }

    /**
     * A helper method to format prices.
     *
     * For the most part, this just forwards the call to PHP's built-in
     * `number_format` function, with an important distinction - the price is
     * first normalized to its real value, based on how many decimal places are
     * stored in the database.
     *
     * @param integer $price
     * @param integer $decimalPlaces
     * @param string $decimalSeparator
     * @param string $thousandsSeparator
     * @return string
     */
    protected function toFormattedString($price, $decimalPlaces, $decimalSeparator, $thousandsSeparator)
    {
        return number_format(
            $price / $this->factor,
            $decimalPlaces,
            $decimalSeparator,
            $thousandsSeparator
        );
    }

    /**
     * Checks whether a supplied string is a valid price, in either of the
     * following formats:
     *
     *     12,345.67
     *     12345.67
     *
     * In other words, we accept a decimal separator (as configured in the
     * instance of this class), optional thousands separators (as configured in
     * the instance of this class), and a maximum specified (or any) number of
     * decimal digits.
     *
     * @param string $price
     * @param string|null $maximumDecimalDigits
     * @param boolean $mandatoryDecimalDigits
     * @param string|null $decimalSeparator
     * @param string|null $thousandsSeparator
     * @return boolean
     */
    public function isValidPrice($price, $maximumDecimalDigits = null, $mandatoryDecimalDigits = false, $decimalSeparator = null, $thousandsSeparator = null)
    {
        if (is_null($decimalSeparator)) {
            $decimalSeparator = $this->decimalSeparator;
        }

        if (is_null($thousandsSeparator)) {
            $thousandsSeparator = $this->thousandsSeparator;
        }

        /*
         * We need to escape the period symbol for the regex, but we don't know
         * which of the separators is which, so we're just gonna excape both,
         * since PHP will know what to do. This should basically handle *any*
         * input correctly.
         */

        $decimalSeparator = preg_quote($decimalSeparator);
        $thousandsSeparator = preg_quote($thousandsSeparator);

        /*
         * First we'll build our pattern for the decimal part, based on the
         * configuration and the arguments.
         */

        $mandatoryDecimalPart = $mandatoryDecimalDigits ? '' : '?';

        if (is_null($maximumDecimalDigits)) {
            $decimalDigits = "\\d+";
        } elseif ($maximumDecimalDigits === 1) {
            $decimalDigits = "\\d";
        } else {
            $decimalDigits = "\\d{1,{$maximumDecimalDigits}}";
        }

        $decimalPart = "({$decimalSeparator}{$decimalDigits}){$mandatoryDecimalPart}";

        /*
         * Now we'll build the two different whole part cases, one without
         * thousands separators, and the second one with them.
         */

        $wholePartWithoutSeparators = "(\\d+)";

        $groupWithSeparators = "({$thousandsSeparator}\\d{3})";
        $groupWithoutSeparators = "(\\d{1,3})";
        $wholePartWithSeparators = "{$groupWithoutSeparators}{$groupWithSeparators}*";

        /*
         * Finally, we'll build the two valid patterns.
         */

        $patternWithoutSeparators = "/^{$wholePartWithoutSeparators}{$decimalPart}$/D";
        $patternWithSeparators = "/^{$wholePartWithSeparators}{$decimalPart}$/D";

        return (
            ((bool) preg_match($patternWithoutSeparators, $price))
            || ((bool) preg_match($patternWithSeparators, $price))
        );
    }

    /**
     * Checks whether the provided string contains the decimal separator.
     *
     * @param string $string
     * @param string|null $decimalSeparator
     * @return boolean
     */
    protected function containsDecimalSeparator($string, $decimalSeparator = null)
    {
        if (is_null($decimalSeparator)) {
            $decimalSeparator = $this->decimalSeparator;
        }

        return strpos($string, $decimalSeparator) !== false;
    }

    /**
     * Converts an amount's stored decimals from one format to another one.
     *
     * @param integer $amount
     * @param integer $from
     * @param integer $to
     * @return integer
     */
    public function convertDecimals($amount, $from, $to)
    {
        $factor = pow(10, abs($from - $to));

        if ($from > $to) {
            return (int) round($amount / $factor);
        } else {
            return $amount * $factor;
        }
    }
}
