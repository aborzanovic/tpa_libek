<?php

namespace Creitive\Html;

use Illuminate\Html\FormBuilder as IlluminateFormBuilder;

class FormBuilder extends IlluminateFormBuilder
{
    /**
     * Renders an "inline" radio button, with the HTML structure for Bootstrap 3.
     *
     * @link http://getbootstrap.com/css/#forms-controls
     * @param string $name
     * @param string $value
     * @param string $label
     * @param boolean $checked
     * @param array $options
     * @return string
     */
    public function inlineRadio($name, $value, $label, $checked, array $options = [])
    {
        return'<label class="radio-inline">'.$this->radio($name, $value, $checked, $options).' '.$label.'</label>';
    }

    /**
     * Renders multiple "inline" radio buttons, with the HTML structure required for
     * Boostrap 3.
     *
     * @link http://getbootstrap.com/css/#forms-controls
     * @param string $name
     * @param array $buttons
     * @param mixed $default
     * @return string
     */
    public function inlineRadios($name, array $buttons, $default = null, array $options = [])
    {
        $output = '';

        /*
         * Resolve which radio button should be selected:
         *
         * 1. If we have data from the posted form, grab that.
         * 2. Otherwise, fallback to the default provided value.
         * 3. If a default value is not provided, select the first radio button.
         */

        if (is_null($default)) {
            $default = key(reset($buttons));
        }

        $selected = $this->old($name);

        if (is_null($selected)) {
            $selected = $default;
        }

        /*
         * Render each of our buttons.
         */

        foreach ($buttons as $value => $label) {
            $output .= $this->inlineRadio($name, $value, $label, ($value === $selected), $options);
        }

        return $output;
    }

    /**
     * Create a date input field.
     *
     * @param string $name
     * @param mixed $value
     * @param array $options
     * @return string
     */
    public function date($name, $value = null, $options = [])
    {
        return $this->input('date', $name, $value, $options);
    }

    /**
     * Create a time input field.
     *
     * @param string $name
     * @param mixed $value
     * @param array $options
     * @return string
     */
    public function time($name, $value = null, $options = [])
    {
        return $this->input('time', $name, $value, $options);
    }

    /**
     * Create a datetime input field.
     *
     * @param string $name
     * @param mixed $value
     * @param array $options
     * @return string
     */
    public function datetime($name, $value = null, $options = [])
    {
        return $this->input('datetime', $name, $value, $options);
    }

    /**
     * Create a number input field.
     *
     * @param string $name
     * @param integer $value
     * @param integer|null $min
     * @param integer|null $max
     * @param array $options
     * @return string
     */
    public function number($name, $value = 0, $min = null, $max = null, $options = [])
    {
        if (!is_null($min)) {
            $options['min'] = $min;
        }

        if (!is_null($max)) {
            $options['max'] = $max;
        }

        return $this->input('number', $name, $value, $options);
    }

    /**
     * Create a select box field.
     *
     * Enables us to also pass an array of options which should be rendered
     * disabled.
     *
     * This is basically copy/pasted from `Illuminate\Html\FormBuilder`, but
     * extended for this functionality.
     *
     * @param string $name
     * @param array $list
     * @param string $selected
     * @param array $options
     * @param array $disabled
     * @return string
     */
    public function select($name, $list = [], $selected = null, $options = [], $disabled = [])
    {
        // When building a select box the "value" attribute is really the selected one
        // so we will use that when checking the model or session for a value which
        // should provide a convenient method of re-populating the forms on post.
        $selected = $this->getValueAttribute($name, $selected);

        $options['id'] = $this->getIdAttribute($name, $options);

        if (!isset($options['name'])) {
            $options['name'] = $name;
        }

        // We will simply loop through the options and build an HTML value for each of
        // them until we have an array of HTML declarations. Then we will join them
        // all together into one single HTML element that can be put on the form.
        $html = [];

        foreach ($list as $value => $display) {
            $html[] = $this->getSelectOption($display, $value, $selected, in_array($value, $disabled));
        }

        // Once we have all of this HTML, we can join this into a single element after
        // formatting the attributes into an HTML "attributes" string, then we will
        // build out a final select statement, which will contain all the values.
        $options = $this->html->attributes($options);

        $list = implode('', $html);

        return "<select{$options}>{$list}</select>";
    }

    /**
     * Get the select option for the given value.
     *
     * Overriden to add the `$disabled` argument.
     *
     * @param string $display
     * @param string $value
     * @param string $selected
     * @param boolean $disabled
     * @return string
     */
    public function getSelectOption($display, $value, $selected, $disabled = false)
    {
        if (is_array($display)) {
            return $this->optionGroup($display, $value, $selected, $disabled);
        }

        return $this->option($display, $value, $selected, $disabled);
    }

    /**
     * Create an option group form element.
     *
     * Overriden to add the `$disabled` argument.
     *
     * @param array $list
     * @param string $label
     * @param string $selected
     * @param boolean $disabled
     * @return string
     */
    protected function optionGroup($list, $label, $selected, $disabled = false)
    {
        $html = [];

        foreach ($list as $value => $display) {
            $html[] = $this->option($display, $value, $selected);
        }

        $disabled = $disabled ? 'disabled' : '';

        return '<optgroup label="'.e($label).'" '.$disabled.'>'.implode('', $html).'</optgroup>';
    }

    /**
     * Create a select element option.
     *
     * Overriden to add the `$disabled` argument.
     *
     * @param string $display
     * @param string $value
     * @param string $selected
     * @param boolean $disabled
     * @return string
     */
    protected function option($display, $value, $selected, $disabled = false)
    {
        $selected = $this->getSelectedValue($value, $selected);

        $options = [
            'value' => e($value),
            'selected' => $selected,
        ];

        $disabled = $disabled ? 'disabled' : '';

        return '<option'.$this->html->attributes($options).' '.$disabled.'>'.e($display).'</option>';
    }
}
