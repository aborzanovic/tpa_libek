<?php

namespace Creitive\Html;

use Creitive\Html\SeoPresenter;

abstract class DefaultSeoPresenter implements SeoPresenter
{
    /**
     * The OpenGraph type.
     *
     * Must be one of the `Opengraph\Opengraph::TYPE_*` constants.
     *
     * @var string
     */
    protected $type = 'website';

    /**
     * The site name.
     *
     * @var string|null
     */
    protected $siteName = null;

    /**
     * An optional locale.
     *
     * @var string|null
     */
    protected $locale = null;

    /**
     * The item title.
     *
     * @var string|null
     */
    protected $title = null;

    /**
     * Whether the title should be overriden completely.
     *
     * @var boolean
     */
    protected $overridesTitle = false;

    /**
     * The item description.
     *
     * @var string|null
     */
    protected $description = null;

    /**
     * An image URL.
     *
     * @var string|null
     */
    protected $image = null;

    /**
     * {@inheritDoc}
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * {@inheritDoc}
     */
    public function hasSiteName()
    {
        return $this->siteName !== null;
    }

    /**
     * {@inheritDoc}
     */
    public function getSiteName()
    {
        return $this->siteName;
    }

    /**
     * {@inheritDoc}
     */
    public function hasLocale()
    {
        return $this->locale !== null;
    }

    /**
     * {@inheritDoc}
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * {@inheritDoc}
     */
    public function hasTitle()
    {
        return $this->title !== null;
    }

    /**
     * {@inheritDoc}
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * {@inheritDoc}
     */
    public function overridesTitle()
    {
        return $this->overridesTitle;
    }

    /**
     * {@inheritDoc}
     */
    public function hasDescription()
    {
        return $this->description !== null;
    }

    /**
     * {@inheritDoc}
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * {@inheritDoc}
     */
    public function hasImage()
    {
        return $this->image !== null;
    }

    /**
     * {@inheritDoc}
     */
    public function getImage()
    {
        return $this->image;
    }
}
