<?php

namespace Creitive\Html;

interface SeoPresenter
{
    /**
     * Gets the type.
     *
     * Must return one of the `Opengraph\Opengraph::TYPE_*` constants.
     *
     * @return string
     */
    public function getType();

    /**
     * Checks whether a site name is configured.
     *
     * @return boolean
     */
    public function hasSiteName();

    /**
     * Returns the configured site name.
     *
     * Returns `null` if no site name is configured.
     *
     * @return string|null
     */
    public function getSiteName();

    /**
     * Checks whether the underlying instance has a specific locale.
     *
     * @return boolean
     */
    public function hasLocale();

    /**
     * Gets the locale.
     *
     * Returns `null` if no locale exists.
     *
     * @return string|null
     */
    public function getLocale();

    /**
     * Checks whether the underlying instance has a title.
     *
     * @return boolean
     */
    public function hasTitle();

    /**
     * Gets the title.
     *
     * Returns `null` if no title exists.
     *
     * @return string|null
     */
    public function getTitle();

    /**
     * Checks whether the underlying instance has a title override.
     *
     * @return boolean
     */
    public function overridesTitle();

    /**
     * Checks whether the underlying instance has a description.
     *
     * @return boolean
     */
    public function hasDescription();

    /**
     * Gets the description.
     *
     * Returns `null` if no description exists.
     *
     * @return string|null
     */
    public function getDescription();

    /**
     * Checks whether the underlying instance has an image.
     *
     * @return boolean
     */
    public function hasImage();

    /**
     * Gets the URL to the image.
     *
     * Returns `null` if no image exists.
     *
     * @return string|null
     */
    public function getImage();
}
