<?php

namespace Creitive\Asset;

use Creitive\Asset\Container;
use Illuminate\Contracts\Filesystem\Filesystem;
use Illuminate\Html\HtmlBuilder;

class Factory
{
    /**
     * The root URL from which the assets are loaded.
     *
     * May be `null`, in which case the assets are loaded using the current
     * domain as the root URL. Allows, for example, serving assets from a
     * different domain.
     *
     * @var string|null
     */
    protected $assetUrl;

    /**
     * A Filesystem implementation.
     *
     * @var \Illuminate\Contracts\Filesystem\Filesystem
     */
    protected $filesystem;

    /**
     * A HtmlBuilder instance.
     *
     * @var \Illuminate\Html\HtmlBuilder
     */
    protected $htmlBuilder;

    /**
     * All of the instantiated asset containers.
     *
     * @var array
     */
    protected $containers = [];

    public function __construct($assetUrl, Filesystem $filesystem, HtmlBuilder $htmlBuilder)
    {
        $this->assetUrl = $assetUrl;
        $this->filesystem = $filesystem;
        $this->htmlBuilder = $htmlBuilder;
    }

    /**
     * Get an asset container instance.
     *
     * <code>
     *     // Get the default asset container
     *     $container = $factory->container();
     *
     *     // Get a named asset container
     *     $container = $factory->container('footer');
     * </code>
     *
     * @param string $container
     * @return \Creitive\Asset\Container
     */
    public function container($container = 'default')
    {
        if (!isset($this->containers[$container])) {
            $this->containers[$container] = new Container(
                $container,
                $this->assetUrl,
                $this->filesystem,
                $this->htmlBuilder
            );
        }

        return $this->containers[$container];
    }

    /**
     * Magic Method for calling methods on the default container.
     *
     * <code>
     *     // Call the "styles" method on the default container
     *     echo $factory->styles();
     *
     *     // Call the "add" method on the default container
     *     $factory->add('jquery', 'js/jquery.js');
     * </code>
     *
     * @param string $method
     * @param array $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        return call_user_func_array([$this->container(), $method], $parameters);
    }
}
