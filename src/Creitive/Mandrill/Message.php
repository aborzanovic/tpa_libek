<?php

namespace Creitive\Mandrill;

use Creitive\Mandrill\Message\Attachment;

class Message
{
    /**
     * The recipients.
     *
     * @var array
     */
    protected $to = [];

    /**
     * The "Reply-To" header.
     *
     * @var array
     */
    protected $replyTo = [
        'email' => '',
        'name' => '',
    ];

    /**
     * The message subject.
     *
     * @var string
     */
    protected $subject = '';

    /**
     * The message body.
     *
     * @var string
     */
    protected $body = '';

    /**
     * An array of file attachments.
     *
     * @var array
     */
    protected $attachments = [];

    /**
     * A helper method to create an instance, for easier method chaining.
     *
     * @return static
     */
    public static function create()
    {
        return new static;
    }

    /**
     * Sets the primary message recipient.
     *
     * @param string $email
     * @param string $name
     * @return $this
     */
    public function to($email, $name = '')
    {
        $this->to[] = [
            'email' => $email,
            'name' => $name,
        ];

        return $this;
    }

    /**
     * Sets the "Reply-To" header.
     *
     * @param string $email
     * @param string $name
     * @return $this
     */
    public function replyTo($email, $name = '')
    {
        $this->replyTo = [
            'email' => $email,
            'name' => $name,
        ];

        return $this;
    }

    /**
     * Sets the message subject.
     *
     * @param string $subject
     * @return $this
     */
    public function subject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Sets the message body.
     *
     * @param string $body
     * @return $this
     */
    public function body($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Attaches a file to the message.
     *
     * @param string $name
     * @param string $path
     * @return $this
     */
    public function attach($name, $path)
    {
        $attachment = new Attachment($name, $path);

        $this->attachments[] = $attachment;

        return $this;
    }

    /**
     * Gets the primary message recipient.
     *
     * @return array
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Gets the "Reply-To" header contents.
     *
     * @return string
     */
    public function getReplyTo()
    {
        return $this->replyTo;
    }

    /**
     * Gets the message subject.
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Gets the message body.
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Gets the attachments.
     *
     * @return array
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * Checks whether this message has "Reply-To" configured.
     *
     * @return boolean
     */
    public function hasReplyTo()
    {
        return (bool) $this->replyTo['email'];
    }
}
