<?php

namespace Creitive\Mandrill\Message;

use Symfony\Component\HttpFoundation\File\File;

class Attachment extends File
{
    /**
     * The virtual name of this attachment.
     *
     * @var string
     */
    protected $virtualName;

    public function __construct($virtualName, $path)
    {
        parent::__construct($path);

        $this->setVirtualName($virtualName);
    }

    /**
     * Sets the virtual name of this attachment.
     *
     * @param string $virtualName
     */
    public function setVirtualName($virtualName)
    {
        $this->virtualName = $virtualName;
    }

    /**
     * Gets the virtual name of this attachment.
     *
     * @return string
     */
    public function getVirtualName()
    {
        return $this->virtualName;
    }

    /**
     * Gets the contents of this attachment.
     *
     * @return string
     */
    public function getContents()
    {
        return file_get_contents($this->getPathname());
    }

    /**
     * Gets base64-encoded contents of this attachment.
     *
     * @return string
     */
    public function getBase64EncodedContents()
    {
        return base64_encode($this->getContents());
    }
}
