<?php

namespace Creitive\Mandrill;

use Creitive\Mandrill\Message;
use Illuminate\Log\Writer as Log;
use Mandrill;
use Mandrill_Error;

/**
 * The Mandrill Mailer class for the Virtual Shopping Center project.
 *
 * A usage example:
 *
 *     $mandrillMailer = new Creitive\Mandrill\Mailer;
 *
 *     $mandrillMessage = new Creitive\Mandrill\Message;
 *     $mandrillMessage->html = '<p>Test test</p>';
 *     $mandrillMessage->subject = 'Mandrill test';
 *     $mandrillMessage->to = array(
 *         'email' => 'milos.levacic@creitive.rs',
 *         'name' => 'Miloš Levačić',
 *     );
 *
 *     $mandrillMailer->send($mandrillMessage);
 *
 * For checking whether the message was successfully sent, read the
 * documentation for the `Mailer::send()` method.
 */
class Mailer
{
    /**
     * A Mandrill instance.
     *
     * @var \Mandrill
     */
    protected $mandrill;

    /**
     * A Log writer.
     *
     * @var \Illuminate\Log\Writer
     */
    protected $log;

    protected $fromEmail;

    protected $fromName;

    protected $replyToEmail;

    protected $replyToName;

    protected $async;

    protected $ipPool;

    public function __construct(Mandrill $mandrill, Log $log, $fromEmail, $fromName, $replyToEmail, $replyToName, $async = false, $ipPool = 'Main Pool')
    {
        $this->mandrill = $mandrill;
        $this->log = $log;
        $this->fromEmail = $fromEmail;
        $this->fromName = $fromName;
        $this->replyToEmail = $replyToEmail;
        $this->replyToName = $replyToName;
        $this->async = $async;
        $this->ipPool = $ipPool;
    }

    /**
     * Sets the "Reply-To" email address.
     *
     * @param string $replyToEmail
     */
    public function setReplyToEmail($replyToEmail)
    {
        $this->replyToEmail = $replyToEmail;
    }

    /**
     * Sets the "Reply-To" name.
     *
     * @param string $replyToName
     */
    public function setReplyToName($replyToName)
    {
        $this->replyToName = $replyToName;
    }

    /**
     * Sends an e-mail using the Mandrill PHP API.
     *
     * Accepts one `array` argument, which will be passed to the Mandrill's
     * message sending method, adding certain configuration parameters if
     * required. For documentation on the available options, see Mandrill's
     * message sending documentation.
     *
     * Returns `false` on failure, or the return value from Mandrill's message
     * sending method otherwise, which is an `array` of return data.
     *
     * @link https://mandrillapp.com/api/docs/messages.php.html#method=send
     * @param \Creitive\Mandrill\Message $message
     * @return array|boolean
     */
    public function send(Message $message)
    {
        try {
            $messageArray = $this->messageToArray($message);

            return $this->mandrill->messages->send($messageArray, $this->async, $this->ipPool);
        } catch (Mandrill_Error $e) {
            $this->log->error('A Mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage());

            return false;
        }
    }

    /**
     * Converts a Message object into an array that can be passed to Mandrill's
     * PHP API.
     *
     * @param \Creitive\Mandrill\Message $message
     * @return array
     */
    public function messageToArray(Message $message)
    {
        $array = [];

        $array['html'] = $message->getBody();
        $array['subject'] = $message->getSubject();

        $to = $message->getTo();

        $array['to'] = [];

        foreach ($message->getTo() as $to) {
            $array['to'][] = [
                'email' => $to['email'],
                'name' => $to['name'],
                'type' => 'bcc',
            ];
        }

        $array['from_email'] = $this->fromEmail;
        $array['from_name'] = $this->fromName;

        if ($message->hasReplyTo()) {
            $replyTo = $message->getReplyTo();

            $array['headers'] = [
                'Reply-To' => $this->createReplyToHeader($replyTo['email'], $replyTo['name']),
            ];
        } elseif ($this->replyToEmail) {
            $array['headers'] = [
                'Reply-To' => $this->createReplyToHeader($this->replyToEmail, $this->replyToName),
            ];
        }

        $attachments = $message->getAttachments();

        if ($attachments) {
            $array['attachments'] = [];
        }

        foreach ($attachments as $attachment) {
            $array['attachments'][] = [
                'type' => $attachment->getMimeType(),
                'name' => $attachment->getVirtualName(),
                'content' => $attachment->getBase64EncodedContents(),
            ];
        }

        return $array;
    }

    /**
     * Creates the "Reply-To" header contents.
     *
     * @param string $email
     * @param string $name
     * @return string
     */
    public function createReplyToHeader($email, $name)
    {
        $email = '<'.$email.'>';

        if (!$name) {
            return $email;
        }

        $name = $this->sanitizeName($name);

        return "\"{$name}\" {$email}";
    }

    /**
     * Name sanitization method.
     *
     * @link http://stackoverflow.com/a/8072733
     * @param string $name
     * @return string
     */
    public function sanitizeName($name)
    {
        $rules = [
            "\r" => '',
            "\n" => '',
            "\t" => '',
            '"'  => "'",
            '<'  => '[',
            '>'  => ']',
        ];

        return trim(strtr($name, $rules));
    }

    /**
     * Creates a new Message instance.
     *
     * @return \Creitive\Mandrill\Message
     */
    public function newMessage()
    {
        return new Message;
    }
}
