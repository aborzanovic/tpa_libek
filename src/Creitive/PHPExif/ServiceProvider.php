<?php

namespace Creitive\PHPExif;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use PHPExif\Adapter\AdapterInterface;
use PHPExif\Adapter\Native as NativeAdapter;
use PHPExif\Reader\Reader;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function register()
    {
        $this->registerAdapter();
        $this->registerReader();
    }

    /**
     * Registers the native adapter for PHPExif as the default implementation.
     *
     * @return void
     */
    protected function registerAdapter()
    {
        $this->app->bind(AdapterInterface::class, NativeAdapter::class);
    }

    /**
     * Registers a singleton reader instance.
     *
     * @return void
     */
    protected function registerReader()
    {
        $this->app->singleton(Reader::class, function ($app) {
            $adapter = $app[AdapterInterface::class];

            return new Reader($adapter);
        });
    }
}
