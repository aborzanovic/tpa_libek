<?php

namespace Creitive\Database\Eloquent;

use Creitive\Database\Eloquent\Collection;
use Creitive\Models\Traits\CalcFoundRowableTrait;
use Eloquent;

class Model extends Eloquent
{
    use CalcFoundRowableTrait;

    /**
     * {@inheritDoc}
     */
    public function newCollection(array $models = [])
    {
        return new Collection($models);
    }

    /**
     * Finds all items whose ID exists in the input array.
     *
     * `null` is also allowed as an argument, to make it easier to use
     * `Input::get()` without having to provide an empty array as a default
     * value.
     *
     * @param array|null $ids
     * @return \Creitive\Database\Eloquent\Collection
     */
    public static function findByIds($ids)
    {
        if (!$ids) {
            return new Collection;
        }

        return static::whereIn('id', $ids)->get();
    }

    /**
     * Finds all items with an attribute that matches the passed value.
     *
     * @param string $column
     * @param mixed $value
     * @return mixed
     */
    public static function findBy($column, $value)
    {
        $result = static::where($column, '=', $value)->where('deleted_at', null);

        if ($result->count() > 1) {
            return $result->get();
        } else {
            return $result->first();
        }
    }
}
