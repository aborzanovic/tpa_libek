<?php

namespace Creitive\Craftar;

use Creitive\Craftar\Response as CraftarResponse;
use GuzzleHttp\Psr7\Response as GuzzleResponse;

trait ResponseTransformingTrait
{
    /**
     * Transforms the Guzzle response into a Craftar response.
     *
     * @param \GuzzleHttp\Psr7\Response $guzzleResponse
     * @return \Creitive\Craftar\Response
     */
    protected function transformResponse(GuzzleResponse $guzzleResponse)
    {
        return new CraftarResponse(
            $guzzleResponse->getStatusCode(),
            $guzzleResponse->getHeaders(),
            $guzzleResponse->getBody()->getContents()
        );
    }
}
