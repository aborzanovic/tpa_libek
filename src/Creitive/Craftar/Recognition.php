<?php
// (C) Catchoom Technologies S.L.
// Licensed under the MIT license.
// https://github.com/Catchoom/craftar-php/blob/master/LICENSE
// All warranties and liabilities are disclaimed.

namespace Creitive\Craftar;

use Creitive\Craftar\ResponseTransformingTrait;
use GuzzleHttp\ClientInterface;

class Recognition
{
    use ResponseTransformingTrait;

    /**
     * A ClientInterface implementation.
     *
     * @var \GuzzleHttp\ClientInterface
     */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * Performs an image recognition search with the passed image.
     *
     * @param string $imageBlob
     * @param array $query
     * @return \Creitive\Craftar\Response
     */
    public function search($imageBlob, $query = [])
    {
        $options = [
            'query' => $query,
            'multipart' => [
                [
                    'name' => 'image',
                    'contents' => $imageBlob,
                    /*
                     * It doesn't matter what the filename is, as long as there
                     * **is** a filename - otherwise, we get errors from the
                     * CraftAR API.
                     */
                    'filename' => 'image.jpg',
                ],
            ],
        ];

        return $this->transformResponse(
            $this->client->post('search', $options)
        );
    }

    /**
     * Gets the Catchoom server timestamp.
     *
     * @return \Creitive\Craftar\Response
     */
    public function timestamp()
    {
        return $this->transformResponse(
            $this->client->post('timestamp')
        );
    }
}
