<?php

namespace Creitive\Http\Middleware\ForceSsl;

use Creitive\Http\Middleware\ForceSsl;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function register()
    {
        $this->app->singleton(ForceSsl::class, function ($app) {
            $forceSsl = $app['config']->get('app.forceSsl');

            return new ForceSsl($forceSsl, $app['redirect']);
        });
    }
}
