<?php

namespace Creitive\Api\Middleware;

use App\Auth\User;
use App\Auth\User\Repository as UserRepository;
use Cartalyst\Sentinel\Sentinel;
use Closure;
use Creitive\Api\DateHeaderValidator;
use Creitive\Api\Exceptions\InvalidToken;
use Creitive\Api\Exceptions\InvalidUserException;
use Creitive\Api\UserHasher;
use Illuminate\Http\Request;

class LoginApiUser
{
    /**
     * User hasher class instance used to recreate hash used to autenticate API
     * user.
     *
     * @var \Creitive\Api\UserHasher\UserHasher
     */
    protected $userHasher;

    /**
     * A Sentinel instance.
     *
     * @var \Cartalyst\Sentinel\Sentinel
     */
    protected $sentinel;

    /**
     * A user repository instance.
     *
     * @var \App\Auth\User\Repository
     */
    protected $userRepository;

    /**
     * A date header validator instance.
     *
     * @var \Creitive\Api\DateHeaderValidator\DateHeaderValidator
     */
    protected $dateHeaderValidator;

    public function __construct(UserHasher $userHasher, Sentinel $sentinel, UserRepository $userRepository, DateHeaderValidator $dateHeaderValidator)
    {
        $this->userHasher = $userHasher;
        $this->sentinel = $sentinel;
        $this->userRepository = $userRepository;
        $this->dateHeaderValidator = $dateHeaderValidator;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $this->authenticateApiUser($request);

        return $next($request);
    }

    /**
     * Authenticates the user via API, in case the correct headers are set.
     *
     * Logs the user into the app via Sentinel, so the user will be available
     * later on, no matter which method was used for logging in.
     *
     * @return void
     */
    protected function authenticateApiUser(Request $request)
    {
        if (!$request->headers->has('X-User-Email')) {
            return;
        }

        $this->dateHeaderValidator->validate();

        $user = $this->userRepository->findByEmail($request->header('X-User-Email'));

        if (!$user instanceof User) {
            throw new InvalidUserException;
        }

        $this->userRepository->touch($user);

        if (!$request->headers->has('X-Authorize')) {
            throw new InvalidToken;
        }

        $xVerification = $request->header('X-Authorize');
        $dateHeader = $request->header('Date');

        $verification = $this->userHasher->getHash($user, $dateHeader);

        if ($verification !== $xVerification) {
            throw new InvalidToken;
        }

        $this->sentinel->login($user);
    }
}
