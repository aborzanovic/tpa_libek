<?php

namespace Creitive\Api;

use Illuminate\Http\Request;

class ApiDetector
{
    /**
     * A instance of the current request.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Whether the current request is an API request (identified by the
     * appropriate header).
     *
     * @var boolean
     */
    protected $isApi = false;

    /**
     * Requests from Android app are supposed to be detected by
     * X-Client-Platform header with value of 'Android'
     *
     * @var boolean
     */
    protected $androidClient = false;

    /**
     * Requests from iOS app are supposed to be detected by `X-Client-Platform`
     * header with value of 'iOS'.
     *
     * @var boolean
     */
    protected $iosClient = false;

    /**
     * Value of a X-Client-Platform header
     *
     * @var boolean
     */
    protected $deviceType = '';

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;

        $this->deviceType = $this->request->header('X-Client-Platform');
        $this->androidClient = ($this->deviceType === 'Android');
        $this->iosClient = ($this->deviceType === 'iOS');

        $this->isApi = ($this->androidClient || $this->iosClient);
    }

    /**
     * Checks whether the current request is an API request.
     *
     * @return boolean
     */
    public function isApi()
    {
        return $this->isApi;
    }

    /**
     * Checks if request was made from android app
     *
     * @return boolean
     */
    public function isAndroid()
    {
        return $this->androidClient;
    }

    /**
     * Checks if request was made from IOS app
     *
     * @return boolean
     */
    public function isIos()
    {
        return $this->iosClient;
    }

    /**
     * Returns current device type
     *
     * @return string
     */
    public function getDeviceType()
    {
        switch ($this->deviceType) {
            case 'Android':
                return 'android';
                break;

            case 'iOS':
                return 'ios';
                break;

            default:
                return '';
                break;
        }
    }
}
