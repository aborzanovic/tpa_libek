<?php

namespace Creitive\Api;

use App\Auth\User;

class UserHasher
{
    /**
     * Generates hash from request data, used to authenticate user
     *
     * @param \App\Auth\User $user
     * @param string $dateHeader
     * @return string
     */
    public function getHash($user, $dateHeader)
    {
        return 'TokenHash ' . hash_hmac(
            'md5',
            $dateHeader,
            $user->auth_token
        );
    }
}
