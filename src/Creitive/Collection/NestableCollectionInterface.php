<?php

namespace Creitive\Collection;

interface NestableCollectionInterface
{
    /**
     * Nests the collection based on the `parent_id` values of individual items.
     *
     * @return \Creitive\Collection\NestableCollectionInterface
     */
    public function nest();
}
