<?php

namespace Creitive\Collection;

interface SluggableCollectionInterface
{
    /**
     * Finds the item in the collection with the specified slug.
     *
     * Only searches the root-level items in this collection.
     *
     * Returns the default value if the item isn't found.
     *
     * @param string $slug
     * @param mixed $default
     * @return \Creitive\Models\SluggableInterface|mixed
     */
    public function findBySlug($slug, $default = null);
}
