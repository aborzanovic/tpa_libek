<?php

namespace Creitive\Collection\Traits;

use LengthException;

trait NestableSluggableCollectionTrait
{
    /**
     * {@inheritDoc}
     */
    public function findBySlugPath(array $path, $default = null)
    {
        if (empty($path)) {
            throw new LengthException;
        }

        $slug = array_shift($path);

        $item = $this->findBySlug($slug);

        if (is_null($item)) {
            return $default;
        }

        if (empty($path)) {
            return $item;
        }

        return $item->children->findBySlugPath($path, $default);
    }
}
