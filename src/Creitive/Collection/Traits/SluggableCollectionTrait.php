<?php

namespace Creitive\Collection\Traits;

use Creitive\Models\SluggableInterface;

trait SluggableCollectionTrait
{
    /**
     * {@inheritDoc}
     */
    public function findBySlug($slug, $default = null)
    {
        return $this->first(function ($key, SluggableInterface $item) use ($slug) {
            return $item->slug === $slug;
        }, $default);
    }
}
