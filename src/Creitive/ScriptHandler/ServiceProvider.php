<?php

namespace Creitive\ScriptHandler;

use Creitive\Asset\Factory as AssetFactory;
use Creitive\ScriptHandler\ScriptHandler;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function register()
    {
        $this->app->singleton(ScriptHandler::class, function ($app) {
            $app['view']->addNamespace('creitive/scripts', __DIR__.'/views');

            return new ScriptHandler(
                $app->environment(),
                $app[AssetFactory::class],
                $app['view'],
                $app['config']
            );
        });
    }
}
