<?php

namespace Creitive\ViewData;

use Creitive\ViewData\ViewData;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function register()
    {
        $this->app->singleton(ViewData::class, function () {
            return new ViewData;
        });

        $this->app['view']->composer('*', 'Creitive\ViewData\Composer');
    }
}
