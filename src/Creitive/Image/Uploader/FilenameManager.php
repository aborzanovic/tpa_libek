<?php

namespace Creitive\Image\Uploader;

use InvalidArgumentException;
use Symfony\Component\HttpFoundation\File\File;

class FilenameManager
{
    /**
     * Generates a random filename (useful for unique filenames). It includes
     * both the current timestamp, and a random part.
     *
     * @param integer $maxLength
     * @return string
     */
    public function generateRandomFilename($maxLength = 16)
    {
        $filename = time().'.'.md5(time().mt_rand());

        return substr($filename, 0, $maxLength);
    }

    /**
     * Compiles a filename based on the passed parts.
     *
     * @param string $prefix
     * @param string $name
     * @param string $suffix
     * @param string $extension
     * @return string
     */
    public function compileNewFilename(
        $prefix = 'image',
        $name = 'no-name',
        $version = '',
        $variant = '',
        $extension = 'png'
    ) {
        $parts = [];

        $parts[] = $prefix;
        $parts[] = $name;
        $parts[] = $version;

        if ($variant) {
            $parts[] = $variant;
        }

        $path = implode('/', $parts);

        if ($extension) {
            return "{$path}.{$extension}";
        } else {
            return $path;
        }
    }
}
