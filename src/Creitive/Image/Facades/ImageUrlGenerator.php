<?php

namespace Creitive\Image\Facades;

use Creitive\Image\UrlGenerator;
use Illuminate\Support\Facades\Facade;

class ImageUrlGenerator extends Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return UrlGenerator::class;
    }
}
