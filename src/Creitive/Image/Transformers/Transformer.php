<?php

namespace Creitive\Image\Transformers;

use Imagick;

interface Transformer
{
    /**
     * Transforms and image and returns it.
     *
     * @param \Imagick $image
     * @param array $parameters
     * @return \Imagick
     */
    public function transform(Imagick $image, array $parameters);
}
