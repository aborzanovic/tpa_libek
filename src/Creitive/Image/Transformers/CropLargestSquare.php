<?php

namespace Creitive\Image\Transformers;

use Creitive\Image\Transformers\Transformer;
use Imagick;

/**
 * Crops the largest possible square from the passed image.
 *
 * The square will be centered both horizontally and vertically within the
 * image.
 */
class CropLargestSquare implements Transformer
{
    /**
     * {@inheritDoc}
     */
    protected function transform(Imagick $image, array $parameters)
    {
        $width = $image->getImageWidth();
        $height = $image->getImageHeight();

        $squareSideLength = min($width, $height);

        if ($squareSideLength === $width) {
            $x = 0;
            $y = (int) ($height / 2 - $squareSideLength / 2);
        } else {
            $x = (int) ($width / 2 - $squareSideLength / 2);
            $y = 0;
        }

        return $image->crop($squareSideLength, $squareSideLength, $x, $y);
    }
}
