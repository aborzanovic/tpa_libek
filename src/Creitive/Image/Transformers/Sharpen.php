<?php

namespace Creitive\Image\Transformers;

use Creitive\Image\Transformers\Transformer;
use Imagick;

/**
 * Sharpens the image based on the passed `radius` and `sigma` values.
 *
 * If they aren't provided, some sane default values will be used.
 */
class Sharpen implements Transformer
{
    /**
     * {@inheritDoc}
     */
    public function transform(Imagick $image, array $parameters)
    {
        $radius = isset($parameters['radius']) ? $parameters['radius'] : 2;
        $sigma = isset($parameters['sigma']) ? $parameters['sigma'] : 1;

        $tmpImage = clone $image;

        $tmpImage->sharpenImage($radius, $sigma);

        return $tmpImage;
    }
}
