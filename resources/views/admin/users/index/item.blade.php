<?php

$editUrl = URL::action('App\Auth\Http\Controllers\Admin\User\Controller@edit', ['user' => $user->id]);
$deleteUrl = URL::action('App\Auth\Http\Controllers\Admin\User\Controller@confirmDelete', ['user' => $user->id]);

$roles = $user->roles->implode('name', ', ');

?>

<li class="itemList-item" data-id="{{ $user->id }}">
    <div class="itemList-item-contents">
        <div class="col-xs-1 itemList-column">
            {{ $user->id }}
        </div>
        <div class="col-xs-3 itemList-column">
            {{ $user->full_name }}
        </div>
        <div class="col-xs-3 itemList-column">
            {{ $user->email }}
        </div>
        <div class="col-xs-2 itemList-column">
            {{ $roles }}
        </div>

        <div class="col-xs-3 itemList-column itemList-noDrag">
            <button type="button" class="visible-xs btn btn-md btn-primary itemList-column-btn rippleButton" data-toggle="dropdown" data-target="#itemList-icons--{{ $user->id }}">
                <i class="fa fa-bars"></i>
            </button>
            <div class="itemList-icons dropdown pull-right" id="itemList-icons--{{ $user->id }}">
                <a href="{{ $editUrl }}" class="itemList-iconLink itemList-iconLink--dropdown rippleButton" data-ripple-color="#ccc">
                    <i class="fa fa-pencil"></i>
                    <span>{{ trans('admin/users.index.links.edit') }}</span>
                </a>
                <a href="{{ $deleteUrl }}" class="itemList-iconLink itemList-iconLink--dropdown rippleButton" data-ripple-color="#ccc">
                    <i class="fa fa-trash-o"></i>
                    <span>{{ trans('admin/users.index.links.delete') }}</span>
                </a>
            </div>
        </div>
    </div>
</li>
