<header class="header">
    <a href="#" class="header-navToggleButton rippleButton">
        <i class="fa fa-arrow-right"></i>
        <i class="fa fa-arrow-left"></i>
    </a>
    <div class="header-content">
        <p class="header-hello">{{{ trans('admin/header.hello', ['name' => $currentUser->full_name ?: $currentUser->email]) }}}</p>

        @include('admin.header.logout')
    </div>
</header>
