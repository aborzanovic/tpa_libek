@foreach ($meta as $property => $content)
    <meta property="{{ $property }}" content="{{ $content }}">
@endforeach
