<noscript class="noscript noscript--cover">
    <div class="noscript--cover-outer">
        <div class="noscript--cover-middle">
            <div class="noscript--cover-inner">
                {{ trans('common.noscriptWarning') }}
            </div>
        </div>
    </div>
</noscript>
