@extends('layouts.front')

@section('content')
    <section class="hero">
        <div class="container">
            <p class="hero-text">Libertarijanski klub - Libek, uz podršku fondacija Global Philantropic Trust i
                Friedrich Naumann Stiftung, raspisuje</p>

            <h2 class="hero-subtitle">konkurs</h2>

            <p class="hero-text">za upis šeste generacije polaznika</p>
            <img src="{{ URL::to('/images/alp-logo.png') }}" width="114" height="135" alt="">

            <h1 class="hero-title">Akademija liberalne politike</h1>
            <a href="{{ URL::action('App\Applications\Http\Controllers\Front\Application\Controller@index') }}"
               class="btn btn-primary">Prijavi se!</a>
        </div>
    </section>

    <section class="intro">
        <div class="container">
            <article class="col-xs-12">

                <p class="text">S velikim zadovoljstvom i ponosom predstavljamo vam šestu Akademiju liberalne politike –
                    unapređeni dvosemestralni program, koji će se odvijati od <strong>oktobra 2016.</strong> do <strong>juna
                        2017.</strong> godine.</p>

                <p class="text">Prijavi se na ALP 6 i budi u prilici da stekneš korisna akademska znanja, kao i niz
                    poslovnih, aktivističkih i istraživačkih veština.</p>

                <p class="text">Postani deo Libekove alumni zajednice i poboljšaj svoje šanse za upis na nekom od
                    renomiranih svetskih univerziteta, za staž u nekom od uticajnijih istraživačkih centara ili
                    zaposlenje u uglednoj domaćoj i stranoj kompaniji.</p>

                <p class="text">Od ove godine, polaznici Akademije imaće specijalan kanal apliciranja za stipendisane
                    Master i PhD programe na CEVRO Institute u Pragu, Texas Tech University i Syracuse University u
                    Sjedinjenim Američkim Državama.</p>
            </article>
            <article class="col-md-6">
                <h1 class="title">Glavni program</h1>

                <p class="text">Ugledni domaći i strani profesori, istraživači, društveni aktivisti i preduzetnici,
                    uvešće vas u svet političke filozofije liberalizma, upoznati sa prednostima tržišne ekonomije i sa
                    vama podeliti svoja viđenja najaktuelnijih globalnih i lokalnih izazova.</p>
            </article>
            <article class="col-md-6">
                <h1 class="title">Mentorski program</h1>

                <p class="text">Mogućnost dodatnog usavršavanja kroz individualni rad sa mentorom iz različitih oblasti
                    privrednog i javnog života zavisno od vaših ličnih i profesionalnih preferencija.</p>
            </article>
        </div>
    </section>

    <section class="coursesIntro--alp6">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h1 class="title">Kreativni program</h1>

                    <p class="text">Libekovi istraživači i saradnici organizovaće za vas čitalačke sekcije, projekcije
                        tematskih igranih i dokumentarnih filmova, nagradno pisanje eseja, i diskutovati o temama kao
                        što su
                        distopija, bioetika, globalni rizici, bezbednost i privatnost. Određeni broj neobaveznih sesija
                        osmišljavaju sami polaznici zavisno od svojih interesovanja. </p>
                    <!--<p class="text">Istraživački kursevi spadaju u obavezne aktivnosti za polaznike ALP 5. U okviru <em>Tehnika istraživanja javnog mnjenja</em>, u prvom, i <em>Pisanja predloga javnih politika</em>, u drugom semestru, pod stalnim mentorstvom Libekovih istraživača, sprovodićete istraživanja javnog mnjenja o relevantnim društvenim pitanjima i pisaćete predloge reformskih javnih politika.</p>
                    <p class="text">U svakom od dva semestra imaćete mogućnosti da pohađate i po jedan novi, specijalizovani teorijski kurs: <em>Kapitalizam i sloboda</em>, u prvom, i <em>Objektivizam John Galt</em>, u drugom. Pohađanje <strong>jednog</strong> od ta dva kursa je obavezno za sve polaznike ALP 5, dok će određeni broj mesta na svakom od dva kursa biti otvoren za odabrane alumniste Libeka.</p>
                    <p class="text">Značajna novina ovogodišnjeg programa jeste i praktični kurs ekonomskog novinarstva u drugom semestru, tokom koga ćete se osposobiti za pisanje i predstavljanje složenih analitičkih sadržaja, a kako biste razvili sposobnost što jasnije argumentacije i unapredili veštine javnog nastupa, višestruko nagrađivani debateri će za vas u prvom semestru organizovati debatne radionice sa atraktivnim temama. Oba kursa su <strong>izborna</strong> – kako za polaznike ALP 5, tako i alumniste Libeka.</p> -->
                </div>
            </div>
        </div>
    </section>

    <section class="courses">
        <div class="courses-row courses-row--large">
            <article class="col-md-6 course course--large course--friedman">
                <h1 class="title">Kapitalizam i sloboda</h1>
                <img src="/images/courses/friedman-logo.png" class="course-image" width="242" height="204"
                     alt="Stay Free to Choose">

                <p class="text">Tokom kursa razmatraćemo centralno pitanje društvenih procesa - zašto se kapitalizam i
                    sloboda nalaze u osnovi svakog razvijenog društva i zašto njihovo odsustvo toliko mnogo košta.</p>
            </article>
            <article class="col-md-6 course course--large course--rand">
                <h1 class="title">Objektivizam John Galt</h1>
                <img src="/images/courses/rand-logo.png" class="course-image" width="234" height="187"
                     alt="Who is John Galt?">

                <p class="text">Odgovarajući na pitanje Ko je Džon Galt upoznaćemo vas sa glavnim pretpostavkama ove
                    filozofske škole libertarijanizma kao i sa njenim praktičnim implikacijama.</p>
            </article>
        </div>

        <section class="researchIntro--alp6">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <h1 class="title">ISTRAŽIVAČKI PROGRAM</h1>

                        <p class="text">U okviru Pisanja predloga javnih politika, u prvom, i Tehnika istraživanja
                            javnog mnjenja, u drugom semestru, pod stalnim mentorstvom Libekovih istraživača,
                            sprovodićete istraživanja javnog mnjenja o relevantnim društvenim pitanjima i pisaćete
                            predloge reformskih javnih politika.</p>
                        <!--<p class="text">Istraživački kursevi spadaju u obavezne aktivnosti za polaznike ALP 5. U okviru <em>Tehnika istraživanja javnog mnjenja</em>, u prvom, i <em>Pisanja predloga javnih politika</em>, u drugom semestru, pod stalnim mentorstvom Libekovih istraživača, sprovodićete istraživanja javnog mnjenja o relevantnim društvenim pitanjima i pisaćete predloge reformskih javnih politika.</p>
                        <p class="text">U svakom od dva semestra imaćete mogućnosti da pohađate i po jedan novi, specijalizovani teorijski kurs: <em>Kapitalizam i sloboda</em>, u prvom, i <em>Objektivizam John Galt</em>, u drugom. Pohađanje <strong>jednog</strong> od ta dva kursa je obavezno za sve polaznike ALP 5, dok će određeni broj mesta na svakom od dva kursa biti otvoren za odabrane alumniste Libeka.</p>
                        <p class="text">Značajna novina ovogodišnjeg programa jeste i praktični kurs ekonomskog novinarstva u drugom semestru, tokom koga ćete se osposobiti za pisanje i predstavljanje složenih analitičkih sadržaja, a kako biste razvili sposobnost što jasnije argumentacije i unapredili veštine javnog nastupa, višestruko nagrađivani debateri će za vas u prvom semestru organizovati debatne radionice sa atraktivnim temama. Oba kursa su <strong>izborna</strong> – kako za polaznike ALP 5, tako i alumniste Libeka.</p> -->
                    </div>
                </div>
            </div>
        </section>


        <div class="courses-row courses-row--small courses-row-height">
            <article class="col-md-6 course course--small--alp6 course--journalism--alp6">
                <h1 class="title">Ekonomsko novinarstvo</h1>

                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <p class="text text-course">Praktični kurs ekonomskog novinarstva, tokom koga ćete se osposobiti
                            za pisanje i
                            predstavljanje složenih analitičkih sadržaja.</p>
                    </div>
                </div>
            </article>
            <article class="col-md-6 course course--small--alp6 course--debate--alp6" style = "margin-left: 2px;">
                <h1 class="title">Debata i javni nastup</h1>

                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <p class="text text-course">Kako biste razvili sposobnost što jasnije argumentacije i unapredili
                            veštine javnog
                            nastupa, višestruko nagrađivani debateri organizovaće za vas debatne radionice sa
                            atraktivnim i
                            društveno aktuelnim temama.</p>
                    </div>
                </div>
            </article>

        </div>
    </section>

    <section class="info">
        <div class="container">

            <h1 class="title">Uslovi</h1>

            <p class="text">Pravo prijave na konkurs za program ALP 6 ostvaruju svi studenti univerziteta u Srbiji.</p>

            <p class="text">
                Mesečna rata kotizacije iznosi <strong>2.400 din.</strong> Ukupno postoji devet mesečnih kotizacija,
                raspoređenih u dva semestra od oktobra 2016 do juna 2017. Moguće je dogovoriti individualnu dinamiku
                plaćanja mesečnih kotizacija.
                Kotizacija pokriva glavni, mentorski, kreativni i istraživački program kao i ostale kurseve. Kao i do
                sada, dodelićemo dve cele i dve parcijalne stipendije budućim polaznicima ALP 6 posle završenog drugog
                kruga selekcije.
            </p>

            <p class="text">Konkurs za ALP 6 otvoren je do <strong>24. septembra</strong>.</p>

            <p class="text">Program na Akademiji počeće <strong>4. oktobra</strong> predstavljanjem celokupnog
                dvosemestralnog programa odabranim polaznicima. Kandidate koji uspešno prođu prvi krug selekcije -
                evaluaciju elektronske prijave, očekuje intervju pred komisijom.</p>

            <p class="text">Nastava će se odvijati u popodnevnim terminima svake nedelje utorkom i četvrtkom u Libekovim
                prostorijama.</p>

            <p class="text">Za sve dodatne informacije pišite nam na <a href="mailto:nikola.ristic@libek.org.rs">nikola.ristic@libek
                    .org.rs</a>.</p>

            <p class="text">Prijavite se!</p>
        </div>
    </section>

    <section class="testimonials owl-carousel owl-theme">
        @foreach (trans('testimonials') as $testimonial)
            <article class="testimonial item">
                <div class="container">
                    <img src="/images/testimonials/{{ $testimonial['slug'] }}.png" width="193" height="193"
                         alt="{{ $testimonial['name'] }}">

                    <p class="text">{{ $testimonial['message'] }}</p>
                    <hgroup>
                        <h1 class="testimonial-name">{{ $testimonial['name'] }}</h1>

                        <h2 class="testimonial-signature">{{ $testimonial['signature'] }}</h2>
                    </hgroup>
                </div>
            </article>
        @endforeach
    </section>

    <footer class="footer">
        <section class="footer-content">
            <img src="{{ URL::to('/images/alp-logo.png') }}" class="footer-alpLogo" width="114" height="135" alt="">

            <h1 class="footer-title">ALP6</h1>
            <a href="{{ URL::action('App\Applications\Http\Controllers\Front\Application\Controller@index') }}"
               class="btn btn-primary">Prijavi se!</a>
        </section>
        <section class="footer-logos">
            <a class="footer-logo footer-logo--libek" href="https://libek.org.rs/">Libertarijanski klub - Libek</a>
            <a class="footer-logo footer-logo--naumann" href="http://www.westbalkan.fnst.org/">Friedrich-Naumann-Stiftung
                für die Freiheit</a>
        </section>
    </footer>
@stop
