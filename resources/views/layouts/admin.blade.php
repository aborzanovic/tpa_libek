@extends('layouts.master')

@section('layoutContent')

    @include('admin.header')

    @include('admin.navigation')

    <section class="contentContainer">
        @yield('content')
    </section>

@stop
