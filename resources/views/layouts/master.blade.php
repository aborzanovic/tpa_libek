<!DOCTYPE html>
<!--[if lt IE 7]> <html lang="{{ $currentLocale }}" class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html lang="{{ $currentLocale }}" class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html lang="{{ $currentLocale }}" class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="{{ $currentLocale }}"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta content="True" name="HandheldFriendly">
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <title>{{ $pageTitle }}</title>
    <meta name="description" content="{{ $meta->description }}">

    <meta name="csrf-token" content="{{ Session::token() }}">

    @if (!App::environment('production'))
        <meta name="environment" content="{{ App::environment() }}">
    @endif

    @include('partials.meta', ['meta' => $meta->opengraph])
    @include('partials.meta', ['meta' => $meta->applinks])
    @include('partials.shortcut-icons')

    {!! $assets->container('head') !!}

</head>
<body class="{{ $bodyClasses }}" data-page="{{ $bodyDataPage }}">
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-6357769-3', 'auto');
    ga('send', 'pageview');
</script>


@include('partials.noscript.bottom')

{!! $assets->container('bodyStart') !!}

@yield('layoutContent')

{!! $assets->container('bodyEnd') !!}

</body>

</html>
