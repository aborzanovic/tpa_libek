@extends('layouts.front')

@section('content')

<?php

$url = URL::action('App\Applications\Http\Controllers\Front\Application\Controller@store');

?>

<style>
    .text-align-center
    {
        text-align: center;
    }
</style>

<section class="hero" style="background-image: url(/images/tpa-hero.jpg); background-size: cover; background-position-y: 25%">
    <div class="container">

        <div> <img src="../../public/images/header-logo.png"> </div>

        <h1><b> TRENING POLITIČKE AKCIJE </b></h1>

        <h2><b> Otvoren konkurs </b></h2>

    </div>
</section>

<section class="form-container">
    <div class="container">
        @if (isset($errors) && !$errors->isEmpty())
            <div class="alert alert-danger">{{ trans('common.genericFormError') }}</div>
        @endif

        <div class="col-md-8 col-md-offset-2">
        {!! Form::open(['url' => $url, 'role' => 'form', 'files' => true]) !!}

            {{-- Personal info --}}


                {{-- Full name --}}

                <div class="form-group text-align-center">
                    {!! Form::label('full_name', trans('applications.labels.fullName')) !!}
                    {!! Form::text('full_name', null, ['class' => 'form-control form-control--application-form']) !!}
                </div>

                @if ($errors->has('full_name'))
                    <div class="alert alert-danger">{!! trans('applications.errors.fullName') !!}</div>
                @endif


                {{-- Birthdate --}}

                <div class="form-group text-align-center">
                    {!! Form::label('birthdate', trans('applications.labels.birthdate')) !!}
                    {!! Form::text('birthdate', null, ['class' => 'form-control form-control--application-form']) !!}
                </div>


                {{-- Sex --}}




                {{-- CV --}}



            {{-- Contact info --}}



                {{-- Email address --}}

                <div class="form-group text-align-center">
                    {!! Form::label('email', trans('applications.labels.email')) !!}
                    {!! Form::email('email', null, ['class' => 'form-control form-control--application-form']) !!}
                </div>

                @if ($errors->has('email'))
                    <div class="alert alert-danger">{!! trans('applications.errors.email') !!}</div>
                @endif


                {{-- Phone --}}


            {{-- Education --}}



                {{-- Faculty --}}

                <div class="form-group text-align-center">
                    {!! Form::label('faculty', trans('applications.labels.faculty')) !!}
                    {!! Form::text('faculty', null, ['class' => 'form-control form-control--application-form']) !!}
                </div>

            {{-- Future-Faculty --}}

            <div class="form-group text-align-center">
                {!! Form::label('future-faculty', trans('applications.labels.future-faculty')) !!}
                {!! Form::text('future-faculty', null, ['class' => 'form-control form-control--application-form']) !!}
            </div>


                {{-- Area of study --}}





                {{-- Impressions --}}


                {{-- Libek history --}}



                {{-- Memberships --}}




            {{-- Personal attitude --}}


                {{-- Problems --}}

                <div class="form-group text-align-center">
                    {!! Form::label('problems', trans('applications.labels.problems')) !!}
                    {!! Form::textarea('problems', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) !!}
                </div>


                {{-- Solutions --}}

                <div class="form-group text-align-center">
                    {!! Form::label('solutions', trans('applications.labels.solutions')) !!}
                    {!! Form::textarea('solutions', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) !!}
                </div>

            {{-- History --}}

            <div class="form-group text-align-center">
                {!! Form::label('history', trans('applications.labels.history')) !!}
                {!! Form::textarea('history', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) !!}
            </div>


                {{-- Future --}}



                {{-- Ego --}}


            {{-- Other --}}


                {{-- Expectations --}}
                <!--
                <div class="form-group">
                    {!! Form::label('expectations', trans('applications.labels.expectations')) !!}
                    {!! Form::textarea('expectations', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) !!}
                </div>


                {{-- Reference --}}

                <div class="form-group">
                    {!! Form::label('reference', trans('applications.labels.reference')) !!}
                    {!! Form::textarea('reference', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) !!}
                </div>


                {{-- Needs --}}

                <div class="form-group">
                    {!! Form::label('needs', trans('applications.labels.needs')) !!}
                    {!! Form::textarea('needs', null, ['class' => 'form-control form-control--application-form', 'rows' => 5]) !!}
                </div>
                -->

            {{-- Submit button --}}

            <div class="form-group">
                {!!
                    Form::button(
                        trans('applications.labels.send.default'),
                        [
                            'class' => 'btn btn-lg btn-success',
                            'type' => 'submit',
                            'data-loading-text' => trans('applications.labels.send.loading'),
                        ]
                    )
                !!}
            </div>

        {!! Form::close() !!}


        <div class="alert alert-info">{!! trans('applications.notes.problems') !!}</div>
        <div class="alert alert-info">{!! trans('applications.notes.privacy') !!}</div>

        </div>

    </div>
</section>

<footer class="footer" style="background: #000000; padding-bottom: 4px">
    <section class="footer-content">
        <img src="{{ URL::to('/images/header-logo.png') }}"  style="margin-top: 20px;">
    </section>

</footer>

@stop
