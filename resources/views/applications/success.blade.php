@extends('layouts.front')

@section('content')

    <section class="hero" style="background-image: url(/images/tpa-hero.jpg); background-size: cover; background-position-y: 25%">
        <div class="container">

            <div> <img src="../../public/images/header-logo.png"> </div>

            <h1><b> TRENING POLITIČKE AKCIJE </b></h1>

            <h2><b> Otvoren konkurs </b></h2>

        </div>
    </section>

<section class="form-container">
    <div class="container">
    <div class="alert alert-success">{!! trans('applications.success') !!}</div>
    </div>
</section>

    <footer class="footer" style="background: #000000; padding-bottom: 4px">
        <section class="footer-content">
            <img src="{{ URL::to('/images/header-logo.png') }}"  style="margin-top: 20px;">
        </section>

    </footer>

@stop
