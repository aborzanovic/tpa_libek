@extends('layouts.front')

@section('content')





    <section class="hero" style="background-image: url(/images/tpa-hero.jpg); background-size: cover; background-position-y: 25%">
        <div class="container">

            <div> <img src="../../public/images/header-logo.png"> </div>

            <h1><b> TRENING POLITIČKE AKCIJE </b></h1>

            <h2> Otvoren konkurs</h2>


            <div class="margin-top">
            <h2>Svim maturantima sa stavom o politici, ekonomiji, pravu.</h2>
            </div>

            <div class="margin-top">
            <a href="{{ URL::action('App\Applications\Http\Controllers\Front\Application\Controller@index') }}"
               class="btn btn-primary boreder-radius" >Prijavi se!</a>
                </div>

            <div class="margin-top">
            <p class="text"><b>Libertarijanski klub LIBEK raspisuje konkurs za TRENING POLITIČKE AKCIJE, koji će se <br> održati u Beogradu, tokom 4 vikenda, u oktobru i novembru.</b></p>
            </div>
        </div>
    </section>

    <style>

        .font-weight
        {
            font-weight: 600;
        }

        .display-table{
            display: table;
            table-layout: fixed;
        }

        .display-cell{
            display: table-cell;
            vertical-align: middle;
            float: none;
        }

        .boreder-radius
        {
            border-radius: 12px;
        }

        .black-button
        {
            color: #000000 !important;
            border-color: #000000 !important;
            margin-top: 25px;
            margin-bottom: 90px;
        }

        .margin-top
        {
            margin-top: 35px;
        }

        .padding-left
        {
            padding-left: 50px;
        }

        .padding-right
        {
            padding-right: 50px;
        }

        .font-p-size
        {
            font-size: 21px;
        }


    </style>

    <section class="intro">
        <div class="container">
            <article class="col-xs-12 col-md-8 col-md-offset-2">

                <p class="text font-weight" >
                    Trening je namenjen đacima završne godine srednjih škola/gimnazija, kao i studentkinjama i studentima prve godine fakulteta,
                    zainteresovanim za oblasti prava, ekonomije, politike, javnog nastupa, komunikacija i medija. Ovim treningom svim učesnicima pružićemo
                    znanja o osnovama ekonomije, političke filozofije, pravu i pravdi. Sa tih osnova, učesnici će među sobom i skladno užim interesovanjima,
                    sa trenerima formirati tri tima u okviru kojih ćemo nastaviti trening:
                </p>
            </article>

        </div>
        </section>

            <div class="row display-table" style="height: 600px; margin-top: 10px;">
                <div class="col-md-6 col-xs-6 coursesIntro--alp6 display-cell" style=" height: 100%; background-image: url(/images/tpa1.jpg) ">

                </div>
                <div class="col-md-5 display-cell" style="">
                    <div class="padding-left" >
                        <h1 style="font-size: 40px;"><b>TIM ANALITIKA</b></h1>
                    <p class="text " style="font-size: 21px;">
                        Stručnjaci sa iskustvom preneće vam svoja znanja o prepoznavanju i analizi društveno-ekonomskih problema.
                        Kako tim problemima prići, postaviti ih u realan kontekst i prezentovati ih javnosti skladno savremenoj
                        istraživačkoj metodologiji? Šta su intervjui, šta ankete, a šta fokus grupe? Šta je alternativa i koji su živi
                        primeri kvalitetnih istraživanja? Na ove teme radićete sa trenerima koji iza sebe imaju više desetina istraživanja.
                    </p>
                        <a href="{{ URL::action('App\Applications\Http\Controllers\Front\Application\Controller@index') }}"
                           class="btn btn-primary boreder-radius black-button" >Prijavi se!</a>

                    </div>


                </div>
                <div class="col-md-1"></div>
            </div>


        <div class="row display-table" style="height: 600px;">

            <div class="col-md-1"></div>
            <div class="col-md-5 display-cell" style="height: 100%; text-align: right">
                <div class="padding-right" >
                    <h1 style="font-size: 40px;"><b>TIM KOMUNIKACIJE</b></h1>
                    <p class="text " style="font-size: 21px;">
                        Često najbolje stvari od društvenog interesa ostaju skrajnute jer su na pogrešan način komunicirane.
                        Šta je kampanja, zašto je važan strateški pristup komunikaciji, kako se komunicira sa medijima i u medijima,
                        a kako učiniti da ljudi bolje usvoje ono što im predstavljate - naučićete sa trenerima koji se profesionalno
                        bave komunikacijama i javnim nastupom, a koji imaju iskustvo u političkim kampanjama i civilnom sektoru.
                    </p>
                    <a href="{{ URL::action('App\Applications\Http\Controllers\Front\Application\Controller@index') }}"
                       class="btn btn-primary boreder-radius black-button" >Prijavi se!</a>

                </div>


                <!--<p class="text">Istraživački kursevi spadaju u obavezne aktivnosti za polaznike ALP 5. U okviru <em>Tehnika istraživanja javnog mnjenja</em>, u prvom, i <em>Pisanja predloga javnih politika</em>, u drugom semestru, pod stalnim mentorstvom Libekovih istraživača, sprovodićete istraživanja javnog mnjenja o relevantnim društvenim pitanjima i pisaćete predloge reformskih javnih politika.</p>
                <p class="text">U svakom od dva semestra imaćete mogućnosti da pohađate i po jedan novi, specijalizovani teorijski kurs: <em>Kapitalizam i sloboda</em>, u prvom, i <em>Objektivizam John Galt</em>, u drugom. Pohađanje <strong>jednog</strong> od ta dva kursa je obavezno za sve polaznike ALP 5, dok će određeni broj mesta na svakom od dva kursa biti otvoren za odabrane alumniste Libeka.</p>
                <p class="text">Značajna novina ovogodišnjeg programa jeste i praktični kurs ekonomskog novinarstva u drugom semestru, tokom koga ćete se osposobiti za pisanje i predstavljanje složenih analitičkih sadržaja, a kako biste razvili sposobnost što jasnije argumentacije i unapredili veštine javnog nastupa, višestruko nagrađivani debateri će za vas u prvom semestru organizovati debatne radionice sa atraktivnim temama. Oba kursa su <strong>izborna</strong> – kako za polaznike ALP 5, tako i alumniste Libeka.</p> -->
            </div>
            <div class="col-md-6 col-xs-6 coursesIntro--alp6 display-cell" style=" height: 100%; background-image: url(/images/tpa2.jpeg) ">

            </div>

        </div>

    <div class="row display-table" style="height: 600px; margin-top: 10px;">
        <div class="col-md-6 col-xs-6 coursesIntro--alp6 display-cell" style=" height: 100%; background-image: url(/images/tpa3.jpg) ">

        </div>
        <div class="col-md-5 display-cell" style="">
            <div class="padding-left" >
                <h1 style="font-size: 40px;"><b>TIM ZAGOVARANJE</b></h1>
                <p class="text " style="font-size: 21px;">
                    Kada shvatimo šta je problem, ponudimo alternativu i prezentujemo je javnosti, ostaje još samo da ono što zagovaramo
                    bude i usvojeno. To ,,samo" je najteže ostvariti. U ovom segmentu razgovaraćemo o tome kako strateški osmisliti kampanju zagovaranja,
                    targetirati ključne aktere, na pravi način pristupiti prodaji politički osetljivog sadržaja donosiocima odluka, podržati tražene
                    zahteve na ulici i naposletku izmeriti uspešnost kampanje. Prikupljanje sredstava i okupljanje partnera, studije uspešnih slučajeva i
                    izazovi za NVO u Srbiji biće tema razgovora sa trenerima sa dugogodišnjim iskustvom u zagovaranju i aktivizmu.
                </p>
                <a href="{{ URL::action('App\Applications\Http\Controllers\Front\Application\Controller@index') }}"
                   class="btn btn-primary boreder-radius black-button black-button" >Prijavi se!</a>

            </div>

            <!--<p class="text">Istraživački kursevi spadaju u obavezne aktivnosti za polaznike ALP 5. U okviru <em>Tehnika istraživanja javnog mnjenja</em>, u prvom, i <em>Pisanja predloga javnih politika</em>, u drugom semestru, pod stalnim mentorstvom Libekovih istraživača, sprovodićete istraživanja javnog mnjenja o relevantnim društvenim pitanjima i pisaćete predloge reformskih javnih politika.</p>
            <p class="text">U svakom od dva semestra imaćete mogućnosti da pohađate i po jedan novi, specijalizovani teorijski kurs: <em>Kapitalizam i sloboda</em>, u prvom, i <em>Objektivizam John Galt</em>, u drugom. Pohađanje <strong>jednog</strong> od ta dva kursa je obavezno za sve polaznike ALP 5, dok će određeni broj mesta na svakom od dva kursa biti otvoren za odabrane alumniste Libeka.</p>
            <p class="text">Značajna novina ovogodišnjeg programa jeste i praktični kurs ekonomskog novinarstva u drugom semestru, tokom koga ćete se osposobiti za pisanje i predstavljanje složenih analitičkih sadržaja, a kako biste razvili sposobnost što jasnije argumentacije i unapredili veštine javnog nastupa, višestruko nagrađivani debateri će za vas u prvom semestru organizovati debatne radionice sa atraktivnim temama. Oba kursa su <strong>izborna</strong> – kako za polaznike ALP 5, tako i alumniste Libeka.</p> -->
        </div>
        <div class="col-md-1"></div>
    </div>




    <section class="info">
        <div class="container">

            <div class="col-md-8 col-md-offset-2">
            <p class="text font-p-size">Nakon dve timske sesije, učesnici se vraćaju na plenarni model rada i sa trenerima zaokružuju i primenjuju naučeno.
                Od polaznika očekujemo da iz ovog programa izađu jači za veštine koje će nakon njega moći da upotrebe u svom okruženju, aktivizmu, karijeri.
            </p>

            <p class="text font-p-size">
                Od sebe očekujemo da vam te veštine prenesemo na posvećen način.
                Očekujte i vi.
            </p>

            <p class="font-p-size"> <strong>Rok za prijavu je: 4. oktobar </strong> </p>



            <p class="text font-p-size">Učešće je besplatno.</p>

            <p class="text font-p-size">Svi polaznici dobiće sertifikat o uspešno završenom Treningu političke akcije.
                Libertarijanski klub Libek</p>

            <a href="{{ URL::action('App\Applications\Http\Controllers\Front\Application\Controller@index') }}"
               class="btn btn-primary boreder-radius black-button black-button" >Prijavi se!</a>

            </div>

        </div>
    </section>

        <!--
    <section class="testimonials owl-carousel owl-theme">
        @foreach (trans('testimonials') as $testimonial)
            <article class="testimonial item">
                <div class="container">
                    <img src="/images/testimonials/{{ $testimonial['slug'] }}.png" width="193" height="193"
                         alt="{{ $testimonial['name'] }}">

                    <p class="text">{{ $testimonial['message'] }}</p>
                    <hgroup>
                        <h1 class="testimonial-name">{{ $testimonial['name'] }}</h1>

                        <h2 class="testimonial-signature">{{ $testimonial['signature'] }}</h2>
                    </hgroup>
                </div>
            </article>
        @endforeach
    </section>-->

    <!--
    <footer class="footer">
        <section class="footer-content">
            <img src="{{ URL::to('/images/ust-logo.png') }}" class="footer-alpLogo" width="114" height="135" alt="" style="margin-top: 11px;">

            <h1 class="footer-title"></h1>
            <a href="{{ URL::action('App\Applications\Http\Controllers\Front\Application\Controller@index') }}"
               class="btn btn-primary">Prijavi se!</a>
        </section>
        <section class="footer-logos">
            <a class="footer-logo footer-logo--libek" href="https://libek.org.rs/">Libertarijanski klub - Libek</a>
            <a class="footer-logo footer-logo--naumann" href="http://www.westbalkan.fnst.org/">Friedrich-Naumann-Stiftung
                für die Freiheit</a>
        </section>
    </footer>
    -->

    <footer class="footer" style="background: #000000; padding-bottom: 4px">
        <section class="footer-content">
            <img src="{{ URL::to('/images/header-logo.png') }}"  style="margin-top: 20px;">
        </section>

    </footer>


@stop
