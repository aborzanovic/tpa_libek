/**
 * Handles the login JS that autofocuses the first input field, and provides a
 * nice fade-in welcome effect for the login form.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');

var creitive = {};

creitive.login = require('creitive/login');
creitive.forms = require('creitive/forms');

/**
 * Initializes the main module.
 *
 * @return {Void}
 */
creitive.initialize = function() {
  creitive.login.initialize();
  creitive.forms.initialize();
};

$(document).ready(function() {
  creitive.initialize();
});
