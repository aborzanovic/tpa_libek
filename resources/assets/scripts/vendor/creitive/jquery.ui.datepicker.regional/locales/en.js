/**
 * English language definitions for the jQuery UI Datepicker plugin.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

( function( $ ) {
    /*jshint sub: true */

    $.datepicker.regional[ "en" ] = {
        closeText: "Close",
        currentText: "Today",
        dateFormat: "dd/mm/yy",
        dayNames: [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ],
        dayNamesMin: [ "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa" ],
        dayNamesShort: [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ],
        firstDay: 0,
        isRTL: false,
        monthNames: [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
        monthNamesShort: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
        nextText: "Next",
        prevText: "Previous",
        showMonthAfterYear: false,
        weekHeader: "Week",
        yearSuffix: ""
    };

})( jQuery );
