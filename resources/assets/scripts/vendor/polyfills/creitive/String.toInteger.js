/**
 * Formats a spread size to a nice human-readable length representation.
 * @return {String}
 */
String.prototype.toInteger = function() {
	return parseInt( this, 10 );
};
