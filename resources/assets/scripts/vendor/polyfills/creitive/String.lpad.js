/**
 * A PHP-like `lpad` method.
 *
 * Taken from the linked page and fixed as per the first comment.
 *
 * @link http://stackoverflow.com/a/10073737
 */

String.prototype.lpad = function( padString, length ) {
	var str = this;

	while (str.length < length) {
		str = padString + str;
	}

	return str.slice(-length);
};
