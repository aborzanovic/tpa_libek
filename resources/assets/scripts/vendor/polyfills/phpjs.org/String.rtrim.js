/**
 * A PHP-like `rtrim` method.
 *
 * Taken from the linked page and adapted as a `String` method.
 *
 * Original function comments:
 *
 * >     // http://kevin.vanzonneveld.net
 * >     // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
 * >     // +      input by: Erkekjetter
 * >     // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
 * >     // +   bugfixed by: Onno Marsman
 * >     // +   input by: rem
 * >     // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
 * >     // *     example 1: rtrim('    Kevin van Zonneveld    ');
 * >     // *     returns 1: '    Kevin van Zonneveld'
 *
 * @link http://phpjs.org/functions/rtrim/
 */

String.prototype.rtrim = function( charlist ) {
	var re;

	charlist = !charlist ? ' \\s\u00A0' : ( charlist + '' ).replace( /([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '\\$1' );

	re = new RegExp( '[' + charlist + ']+$', 'g' );

	return this.replace( re, '' );
};
