/**
 * A handler plugin for the "onbeforeunload" event in browsers.
 *
 * This is the event that gets triggered when the user tries to close the
 * current page, navigate away from it, etc.
 *
 * The plugin enables other plugins to register themselves, and perform
 * block/unblock operations which affect whether the user will be shown a
 * warning when trying to close the page (or navigate away from it, but this is
 * yet to be implemented, as we need to decide on the best strategy for handling
 * this).
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');

/**
 * A unique ID for automatically assigning handler IDs.
 *
 * @type {Number}
 */
var nextUid = 0;

/**
 * An object where we will store all the registered onbeforeunload handlers, and
 * their blocked/unblocked state.
 *
 * @type {Object}
 */
var registeredHandlers = {};

/**
 * Returns a new unique ID, and increments the internal `nextUid` counter.
 *
 * @return {Number}
 */
var getNextUid = function() {
  return nextUid++;
};

/**
 * Checks whether a uid was registered as a handler.
 *
 * @return {Boolean}
 */
var isRegistered = function(uid) {
  return registeredHandlers.hasOwnProperty(uid);
};

/**
 * Returns a new handler object, with a `blocked` state flag, and an optional
 * callback.
 *
 * The callback (if exists) will be called each time the "beforeunload" event
 * fires, and the current state of that handler will be passed to it.
 *
 * @param {Function} callback
 * @return {Object}
 */
var getNewHandler = function(callback) {
  var handler = {
    blocked: false
  };

  if ($.isFunction(callback)) {
    /**
     * Calls the callback, passing it the handler's current blocking status.
     *
     * @return {Function}
     */
    handler.callback = function() {
      callback(handler.blocked);
    };
  } else {
    /**
     * A no-op, in case we have no callback.
     *
     * @return {Function}
     */
    handler.callback = function() {};
  }

  return handler;
};

/**
 * Registers a handler.
 *
 * @param {Number|String} uid
 * @param {Function} callback
 * @return {Number|String}
 */
var register = function(uid, callback) {
  registeredHandlers[uid] = getNewHandler(callback);

  return uid;
};

/**
 * The function that will be subscribed to the "beforeunload" event. It will
 * check whether or not a block currently exists, and, if required, block the
 * tab from being closed. It will also call all the callbacks for each of the
 * registered handlers.
 *
 * @return {Void|String}
 */
var mainHandler = function() {
  var blockExists = false;
  var uid;

  for (uid in registeredHandlers) {
    if (registeredHandlers.hasOwnProperty(uid)) {
      blockExists = blockExists || registeredHandlers[uid].blocked;
      registeredHandlers[uid].callback();
    }
  }

  if (blockExists) {
    return 'You have unsaved changes, are you sure you want to leave?';
  }
};

module.exports = {
  /**
   * Registers a new handler for blocking the page, and returns its `uid`,
   * which can later be used in calls to `block()` and `unblock()`, so that
   * multiple plugins can independently stop the page from being closed.
   *
   * Accepts an optional callback, which will be called each time the
   * `beforeunload` event is fired.
   *
   * If no `uid` is supplied, one will be generated automatically.
   *
   * @param {Number|String} uid
   * @param {Function} callback
   * @return {Number|String}
   */
  register: function(uid, callback) {
    if (typeof uid === 'undefined') {
      uid = getNextUid();

      return register(uid);
    }

    if ($.isFunction(uid)) {
      callback = uid;
      uid = getNextUid();

      return register(uid, callback);
    }

    if (isRegistered(uid)) {
      throw new Error('Duplicate uid registration for onBeforeUnloader!');
    }

    return register(uid, callback);
  },

  /**
   * Blocks the page from closing, for a specified uid.
   *
   * @param {Number|String} uid
   * @return {Void}
   */
  block: function(uid) {
    if (!isRegistered(uid)) {
      throw new Error('Attempted onBeforeUnloader.block() with an unregistered uid!');
    }

    registeredHandlers[uid].blocked = true;
  },

  /**
   * Unblocks the page from closing, for a specified uid.
   *
   * @param {Number|String} uid
   * @return {Void}
   */
  unblock: function(uid) {
    if (!isRegistered(uid)) {
      throw new Error('Attempted onBeforeUnloader.unblock() with an unregistered uid!');
    }

    registeredHandlers[uid].blocked = false;
  },

  /**
   * Initializes the module.
   *
   * @return {Void}
   */
  initialize: function() {
    $(window).on('beforeunload', function() {
      return mainHandler();
    });
  }
};
