/**
 * CSRF handler for the EWS javascript framework.
 *
 * Caches the CSRF token from the appropriate `<meta>` tag into predefined vars,
 * so various AJAX calls could simply read from those, instead of making a DOM
 * lookup each time.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');

/**
 * The name of the CSRF token
 *
 * @type {String}
 */
var tokenName = '_token';

/**
 * The CSRF token value
 *
 * @type {String}
 */
var token;

/**
 * Parses the `meta[name="csrf-token"]` tag in the document's `<head>` to fetch
 * the current CSRF token value.
 *
 * @return {String}
 */
var getTokenFromMeta = function() {
  return $('meta').filterByAttr('name', 'csrf-token').attr('content');
};

module.exports = {
  /**
   * Injects the current token into an object or a query string. This is
   * designed to be used for AJAX requests, so that the CSRF protection can be
   * used there as well.
   *
   * @param {Object|String} object
   * @return {Object|String}
   */
  inject: function(object) {
    if (typeof object === 'object') {
      object[tokenName] = token;
    } else if (typeof object === 'string') {
      object += '&' + tokenName + '=' + token;
    }

    return object;
  },

  /**
   * Gets the current token name.
   *
   * @return {String}
   */
  getTokenName: function() {
    return tokenName;
  },

  /**
   * Gets the current token.
   *
   * @return {String}
   */
  getToken: function() {
    return token;
  },

  /**
   * Initializes the module.
   *
   * @return {Void}
   */
  initialize: function() {
    token = getTokenFromMeta();
  }
};
