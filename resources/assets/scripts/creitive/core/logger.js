/**
 * Logger for the CreITive javascript framework.
 *
 * For now, this just forwards everything to `console.log`, but in the future,
 * we can easily upgrade it to use some online logging platform, without
 * changing any code that actually uses the logger.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

/* global console */

'use strict';

var environment = require('./environment');
var browser = require('./browser');

/**
 * Entries buffered for logging before document.ready has fired.
 *
 * @type {Array}
 */
var bufferedEntries = [];

/**
 * Whether the plugin was initialized or not.
 *
 * This should be set to `true` on `document.ready`.
 *
 * @type {Boolean}
 */
var initialized = false;

/**
 * Logs an entry to the console, if the environment is not "production". We had
 * to add fallback behavior in case of Internet Explorer
 *
 * @param {String} type
 * @param {Array} messages
 * @return {Void}
 */
var logEntry = function(type, messages) {
  if (environment.is('production')) {
    return;
  }

  if (typeof console !== 'undefined') {
    if (!browser.browser.msie) {
      console[type].apply(console, messages);
      return;
    }

    for (var i = 0; i < messages.length; i++) {
      console.log(messages[i]);
    }
  }
};

/**
 * Handles a log entry, depending on whether the document has been initialized.
 *
 * @param {String} type
 * @param {Array} messages
 * @return {Void}
 */
var handleEntry = function(type, messages) {
  if (initialized) {
    logEntry(type, messages);
  } else {
    bufferedEntries.push({
      type: type,
      messages: messages
    });
  }
};

/**
 * Flushes buffered entries to the console.
 *
 * This should only be called once, after the document has been initialized.
 *
 * @return {Void}
 */
var flushBufferedEntries = function() {
  var entry;

  while (bufferedEntries.length) {
    entry = bufferedEntries.shift();

    logEntry(entry.type, entry.messages);
  }
};

module.exports = {

  /**
   * Logs a debug message.
   *
   * @return {Void}
   */
  debug: function() {
    handleEntry('debug', arguments);
  },

  /**
   * Logs an error message.
   *
   * @return {Void}
   */
  error: function() {
    handleEntry('error', arguments);
  },

  /**
   * Logs an info message.
   *
   * @return {Void}
   */
  info: function() {
    handleEntry('info', arguments);
  },

  /**
   * Logs a generic message.
   *
   * @return {Void}
   */
  log: function() {
    handleEntry('log', arguments);
  },

  /**
   * Logs a warning message.
   *
   * @return {Void}
   */
  warn: function() {
    handleEntry('warn', arguments);
  },

  /**
   * Initializes the module.
   *
   * @return {Void}
   */
  initialize: function() {
    flushBufferedEntries();

    initialized = true;
  }

};
