/**
 * Creitive JS library core
 *
 * Imports all core plugins onto a single object, which may later be exposed on
 * the global `window` to allow accessing plugins from the browser console.
 *
 * For individual plugins, however, it is recommended to directly require the
 * needed plugin, instead of the complete library.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var core = {};

core.ajaxLoader = require('./ajaxLoader');
core.api = require('./api');
core.browser = require('./browser');
core.cookieListener = require('./cookieListener');
core.csrf = require('./csrf');
core.environment = require('./environment');
core.errorDialog = require('./errorDialog');
core.keyCode = require('./keyCode');
core.language = require('./language');
core.logger = require('./logger');
core.onBeforeUnloader = require('./onBeforeUnloader');
core.util = require('./util');

/**
 * Initializes the core module.
 *
 * @return {Void}
 */
core.initialize = function() {
  core.ajaxLoader.initialize();
  core.browser.initialize();
  core.csrf.initialize();
  core.environment.initialize();
  core.language.initialize();
  core.logger.initialize();
  core.onBeforeUnloader.initialize();
  core.util.initialize();
};

module.exports = core;
