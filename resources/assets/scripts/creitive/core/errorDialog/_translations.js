/**
 * Translations for the errorDialog module.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

//jscs:disable requireCamelCaseOrUpperCaseIdentifiers
//jshint camelcase:false

'use strict';

module.exports = {
  defaultTitle: {
    en_US: 'Error',
    sr_RS: 'Greška',
    bs_BA: 'Greška'
  },
  defaultMessage: {
    en_US: 'An unexpected error occured. We apologize for the inconvenience. Please contact the administrator about the problem. Thank you for your patience!',
    sr_RS: 'Došlo je do nepoznate greške. Izvinjavamo se na neprijatnosti. Molimo Vas obavestite administratora o problemu. Hvala Vam na razumevanju!',
    bs_BA: 'Došlo je do nepoznate greške. Izvinjavamo se na neprijatnosti. Molimo Vas obavestite administratora o problemu. Hvala Vam na razumevanju!'
  },
  defaultButton: {
    en_US: 'OK',
    sr_RS: 'OK',
    bs_BA: 'OK'
  }
};
