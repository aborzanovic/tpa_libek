/**
 * Creitive CMS login module.
 *
 * Displays a caps lock warning for the password input, and performs a fade-in
 * animation of the login form.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');

var login = {};

/**
 * The password input element.
 *
 * @type {Object}
 */
var passwordInput;

/**
 * Whether caps lock is turned on.
 *
 * @type {Boolean|null}
 */
var capsLockOn = null;

/**
 * Whether the password input is focused.
 *
 * @type {Boolean}
 */
var passwordFocused = false;

/**
 * Displays the caps lock warning if the user is in the password input, and caps
 * lock is turned on - hides it otherwise.
 *
 * @return {Void}
 */
var updateCapsLockWarning = function() {
  if (passwordFocused && capsLockOn) {
    passwordInput.tipsy('show', { html: true });
  } else {
    passwordInput.tipsy('hide');
  }
};

/**
 * Initializes the module.
 *
 * @return {Void}
 */
login.initialize = function() {
  passwordInput = $('input.password-input');

  /*
   * Initialize the capslockstate plugin.
   */
  $(window).capslockstate();

  /**
   * Set up tipsy on the password input.
   */
  passwordInput.tipsy({
    trigger: 'manual',
    gravity: 'w'
  });

  /**
   * Set up caps lock warning events.
   */

  $(window).bind('capsOn', function() {
    capsLockOn = true;
    updateCapsLockWarning();
  });

  $(window).bind('capsOff', function() {
    capsLockOn = false;
    updateCapsLockWarning();
  });

  passwordInput.on('focus', function() {
    passwordFocused = true;
    updateCapsLockWarning();
  });

  passwordInput.on('blur', function() {
    passwordFocused = false;
    updateCapsLockWarning();
  });
};

module.exports = login;
