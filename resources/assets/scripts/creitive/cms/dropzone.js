/**
 * Initializes the Dropzone editor.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');
var Dropzone = require('Dropzone');
var csrf = require('creitive/core/csrf');
var sortable = require('creitive/cms/sortable');

/**
 * The template used for displaying uploaded images.
 *
 * Tips on how to customize this are available at the linked resource.
 *
 * @link http://www.dropzonejs.com
 * @type {String}
 */
var previewTemplate = '<div class="dz-preview dz-file-preview">' +
  '  <div class="dz-details">' +
  '    <img data-dz-thumbnail />' +
  '  </div>' +
  '  <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>' +
  '  <div class="dz-success-mark"><span>✔</span></div>' +
  '  <div class="dz-error-mark"><span>✘</span></div>' +
  '  <div class="dz-error-message"><span data-dz-errormessage></span></div>' +
  '</div>';

/**
 * Adds an image to the specified dropzone.
 *
 * @param {Object} dropzone
 * @param {Object} image
 * @return {Void}
 */
var addExistingImage = function(dropzone, image) {
  dropzone.emit('addedfile', image);
  dropzone.emit('thumbnail', image, image.url);

  var $element = $(image.previewElement);

  $element.attr('data-id', image.id);
};

/**
 * Adds an array of images to the specified dropzone.
 *
 * The images argument must be an array, within which each item must be an
 * object with the properties `filename`, `size`, `url`.
 *
 * @param {Object} dropzone
 * @param {Array} images
 */
var addExistingImages = function(dropzone, images) {
  var existingImageCount = 0;

  images.forEach(function(image) {
    addExistingImage(dropzone, image);
    ++existingImageCount;
  });

  if (dropzone.options.maxFiles !== null) {
    dropzone.options.maxFiles = dropzone.options.maxFiles - existingImageCount;
  }
};

/**
 * Initializes a single Dropzone.
 *
 * @param {Object} element
 * @return {Void}
 */
var initializeDropzone = function(element) {
  var $element = $(element);

  var elementId = $element.attr('id');
  var uploadUrl = $element.attr('data-upload-url');
  var sortUrl = $element.attr('data-sort-url');
  var existingImages = $element.data('existing-images');
  var previewsContainerIdSelector = '#' + $element.attr('data-previews-container');
  var $previewsContainer = $(previewsContainerIdSelector);

  var options = {
    url: uploadUrl,
    paramName: 'image',
    maxFileSize: 20,
    previewsContainer: previewsContainerIdSelector,
    addRemoveLinks: true,
    previewTemplate: previewTemplate
  };

  var dropzone = new Dropzone('#' + elementId, options);

  /**
   * When an image is being uploaded, the CSRF token must be appended.
   */

  dropzone.on('sending', function(file, jqXHR, formData) {
    formData.append(csrf.getTokenName(), csrf.getToken());
  });

  /**
   * When an image is uploaded succesfuly, we add imageId to preview container
   */

  dropzone.on('success', function(file, jqXHR) {
    $(file.previewElement).attr('data-id', jqXHR.id);
    file.id = jqXHR.id;

    //jscs:disable requireCamelCaseOrUpperCaseIdentifiers
    //jshint camelcase:false
    file.deleteUrl = jqXHR.delete_url;
  });

  /**
   * When an image is deleted, a matching request must be sent to the server.
   */

  dropzone.on('removedfile', function(file) {
    var postData = {
      id: file.id
    };

    $.ajax({
      type: 'DELETE',
      url: file.deleteUrl,
      data: csrf.inject(postData)
    });
  });

  /**
   * Initialize the existing images.
   */
  addExistingImages(dropzone, existingImages);

  if ($previewsContainer.hasClass('dropzone-sortable')) {
    sortable($previewsContainer, sortUrl, 'img');
  }

};

module.exports = {
  /**
   * Initializes the Dropzone module.
   *
   * @return {Void}
   */
  initialize: function() {
    $('[data-dropzone]').each(function(index, element) {
      initializeDropzone(element);
    });
  }
};
