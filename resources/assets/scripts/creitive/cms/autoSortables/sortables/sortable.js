/**
 * Initializes regular (non-nested) sorting.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');
var errorDialog = require('creitive/core/errorDialog');

/**
 * The API reference.
 *
 * @type {Object}
 */
var api;

var unsavedSortable = false;

var $container;
var $confirmLink;
var $rootList;
var confirmUrl;

/**
 * Handles changes to the list.
 *
 * @todo Mark items as moved.
 * @return {Void}
 */
var handleChange = function() {
  unsavedSortable = true;
};

/**
 * Handles clicking on the confirm button.
 *
 * @param {Event} event
 * @return {Void}
 */
var handleConfirm = function(event) {
  event.preventDefault();

  $confirmLink.button('loading');

  api.putSort(
    confirmUrl,
    $rootList.nestable('serialize'),
    function() {
      unsavedSortable = false;
    },

    function() {
      errorDialog.show();
    },

    function() {
      $confirmLink.button('reset');
    }

  );
};

module.exports = {
  /**
   * Initializes the sortable logic on the passed container.
   *
   * @param {Object} container
   * @param {Object} apiReference
   * @return {Void}
   */
  initialize: function(container, apiReference) {
    $container = $(container);
    api = apiReference;

    $confirmLink = $container.find('.itemList-confirmLink');
    $rootList = $container.find('.itemList-rootContainer');
    confirmUrl = $confirmLink.attr('data-url');

    $rootList.nestable({
      maxDepth: 1,
      group: 0,
      listNodeName: 'ol',
      itemNodeName: 'li',
      rootClass: 'itemList-rootContainer',
      listClass: 'itemList-list',
      itemClass: 'itemList-item',
      dragClass: 'itemList-item--dragged',
      handleClass: 'itemList-item-contents',
      collapsedClass: 'itemList-item--collapsed',
      placeClass: 'itemList-placeholder',
      noDragClass: 'itemList-noDrag',
      emptyClass: 'itemList-empty'
    });

    $rootList.on('change', handleChange);
    $confirmLink.on('click', handleConfirm);
  }
};
