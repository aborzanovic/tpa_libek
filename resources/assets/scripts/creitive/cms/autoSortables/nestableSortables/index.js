/**
 * Initializes the nestableSortable logic for each instance found on the current
 * page.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');
var nestableSortable = require('./nestableSortable');

module.exports = {
  /**
   * Initializes the module.
   *
   * @param {Object} apiReference
   * @return {Void}
   */
  initialize: function(apiReference) {
    $('.itemList--nestableSortable').each(function(index, container) {
      nestableSortable.initialize(container, apiReference);
    });
  }
};
