/**
 * Ripple effect
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');

/**
 * Ripple button
 *
 * @var {Object}
 */
var rippleButton;

/**
 * Ripple button with effect
 *
 * @var {Object}
 */
var rippleButtonEffect;

/**
 * Button position
 *
 * @var {Number}
 */
var rippleButtonOffset;

/**
 * Sets up the ripple effect when an element is clicked.
 *
 * @param {Object} rippleButton
 * @return {Void}
 */
var initializeRippleEffect = function(rippleButton) {
  rippleButton.on('click', function(event) {
    var $this = $(this);

    rippleButtonOffset = $this.offset();

    var xPos = event.pageX - rippleButtonOffset.left;
    var yPos = event.pageY - rippleButtonOffset.top;

    rippleButtonEffect.addClass('rippleButton-effect');

    rippleButtonEffect.css('height', $this.height());
    rippleButtonEffect.css('width', $this.height());

    rippleButtonEffect
        .css({
          top: yPos - (rippleButtonEffect.height() / 2),
          left: xPos - (rippleButtonEffect.width() / 2),
          background: $this.data('ripple-color')
        })
        .appendTo($this);

    rippleButtonEffect.on('animationend webkitAnimationEnd oanimationend MSAnimationEnd', function() {
      rippleButtonEffect.remove();
    });
  });
};

module.exports = {
  /**
   * Initializes the ripple effect.
   *
   * @return {Void}
   */
  initialize: function() {
    rippleButton = $('.rippleButton');

    rippleButtonEffect = $('<div></div>');

    initializeRippleEffect(rippleButton);
  }
};
