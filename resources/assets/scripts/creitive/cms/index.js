/**
 * Initializes plugins on document.ready
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var cms = {};

cms.autoSortables = require('./autoSortables');
cms.datepicker = require('./datepicker');
cms.dropzone = require('./dropzone');
cms.redactor = require('./redactor');
cms.sidebarNavToggler = require('./sidebarNavToggler');
cms.rippleEffect = require('./rippleEffect');

/**
 * Initializes the CMS module.
 *
 * @param {Object} apiReference
 * @return {Void}
 */
cms.initialize = function(apiReference) {
  cms.autoSortables.initialize(apiReference);
  cms.datepicker.initialize();
  cms.dropzone.initialize();
  cms.redactor.initialize();
  cms.sidebarNavToggler.initialize();
  cms.rippleEffect.initialize();
};

module.exports = cms;
