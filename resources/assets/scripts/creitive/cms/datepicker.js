/**
 * jQuery UI datepicker initialization.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');

module.exports = {
  /**
   * Initializes the datepicker.
   *
   * @return {Void}
   */
  initialize: function() {
    $('.datepicker').datepicker();
  }
};
