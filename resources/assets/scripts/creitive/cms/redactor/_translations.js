/**
 * Translations for the Redactor module.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

//jscs:disable requireCamelCaseOrUpperCaseIdentifiers
//jshint camelcase:false

'use strict';

module.exports = {
  imageUploadError: {
    en_US: 'Image upload error! Please check if you have uploaded the correct file type!',
    sr_RS: 'Greška prilikom slanja slike! Molimo Vas proverite da li ste poslali odgovarajući tip fajla!',
    bs_BA: 'Greška prilikom slanja slike! Molimo Vas proverite da li ste poslali odgovarajući tip fajla!'
  },
  fileUploadError: {
    en_US: 'File upload error! Please check if you have uploaded the correct file type!',
    sr_RS: 'Greška prilikom slanja fajla! Molimo Vas proverite da li ste poslali odgovarajući tip fajla!',
    bs_BA: 'Greška prilikom slanja fajla! Molimo Vas proverite da li ste poslali odgovarajući tip fajla!'
  }
};
