/**
 * Initializes Bootstrap's "loading" status for buttons with the
 * `data-loading-text` attribute configured.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');

module.exports = {

  /**
   * Initializes the module.
   *
   * @return {Void}
   */
  initialize: function() {
    $('button[data-loading-text]').each(function(index, element) {
      /**
       * The button jQuery object.
       *
       * @type {Object}
       */
      var $element = $(element);

      /**
       * The parent form element, if one exists.
       *
       * @type {Object}
       */
      var $form = $element.closest('form');

      /**
       * Whether the button is within a form element.
       *
       * @type {Boolean}
       */
      var isInForm = $form.length;

      /**
       * When the button is clicked, we want to trigger the "loading" status,
       * and disable the button. However, we need to hack this up a bit so it
       * works correctly when a nested button element is clicked (for more
       * info, see the linked issue).
       *
       * Note that the Bootstrap version used when writing this plugin is
       * Bootstrap 3.2.0, but this problem was "fixed" (in a roundabout way)
       * in more recent Bootstrap versions.
       *
       * @link https://github.com/twbs/bootstrap/issues/14450
       * @link https://github.com/twbs/bootstrap/pull/14457
       */
      $element.on('click', function(event) {
        var $target = $(event.target);

        if (!$target.is('button')) {
          event.stopPropagation();

          $target.closest('button').click();

          return;
        }

        /**
         * We'll only trigger the "loading" state in case the button is not in
         * a form, or when the form is valid - otherwise the button will get
         * locked in the "loading" state, and the user won't be able to submit
         * the form in a user-friendly manner.
         */
        if (!isInForm || $form.get(0).checkValidity()) {
          $element.button('loading');
        }
      });

    });
  }

};
