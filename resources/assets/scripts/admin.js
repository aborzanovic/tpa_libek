/**
 * Admin panel scripts.
 *
 * @copyright ©2011-2015 CreITive (http://www.creitive.rs)
 */

'use strict';

var $ = require('jquery');

var apiReference = require('./app/api');

/**
 * Declare all the CreITive scripts.
 */

var creitive = {};

creitive.core = require('creitive/core');
creitive.cms = require('creitive/cms');
creitive.forms = require('creitive/forms');

/**
 * Initializes the main module.
 *
 * @return {Void}
 */
creitive.initialize = function() {
  creitive.core.initialize();
  creitive.cms.initialize(apiReference);
  creitive.forms.initialize();

  /**
   * In a non-production environment, the complete library will be attached to
   * the global `window` object, so as to be able to access it from the
   * browser console.
   */

  if (!creitive.core.environment.is('production')) {
    global.window.creitive = creitive;
  }

  /**
   * As a sanity check for development, we'll log a "Ready" message to the
   * console.
   */

  creitive.core.logger.info('Ready!');
};

/**
 * Initialize everything when the document is ready.
 */

$(document).ready(function() {
  creitive.initialize();
});
