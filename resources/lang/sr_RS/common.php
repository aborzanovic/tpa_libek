<?php

return [

    'siteName' => 'Trening Političke Akcije - Libertarijanski Klub Libek',
    'homeTagline' => '',
    'metaDescription' => 'Prijavi se na Trening Političke Akcije i budi u prilici da stekneš vrlo korisna znanja.',
    'noscriptWarning' => 'Nažalost, Vaš pregledač blokira izvršavanje skriptova. Kako biste koristili ovaj sajt, molimo Vas da dozvolite izvršavanje skriptova u Vašem pregledaču. Hvala za razumevanje.',
    'adminPanel' => 'Admin panel',
    'dateFormat' => 'd.m.Y.',
    'dateTimeFormat' => 'd.m.Y. H:i',
    'decimalSeparator' => ',',
    'thousandsSeparator' => '.',
    'genericFormError' => 'Niste pravilno popunili formu. Molimo Vas ispravite greške i pokušajte ponovo.',

];
