<?php

return [

    'module' => 'Change password',

    'titles' => [
        'update' => 'Change password',
    ],

    'labels' => [
        'currentPassword' => 'Current password',
        'newPassword' => 'New password',
        'newPasswordConfirmation' => 'Repeat new password',
        'update' => [
            'default' => 'Update',
            'loading' => 'Updating...',
        ],
    ],

    'successMessages' => [
        'update' => 'You have successfully updated your password.',
    ],

    'errorMessages' => [
        'update' => [
            'current_password' => [
                'required' => 'You need to enter your current password.',
                'current_password' => 'The password you have entered is incorrect.',
            ],
            'new_password' => [
                'required' => 'You must enter a new password.',
                'confirmed' => 'You must correctly repeat your new password.',
            ],
        ],
    ],

];
