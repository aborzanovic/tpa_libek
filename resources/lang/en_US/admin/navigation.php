<?php

return [

    'home' => 'Home',
    'applications' => 'Applications',
    'users' => 'Users',
    'password' => 'Change password',
    'backToWebsite' => 'Back to website',

];
