<?php

namespace App\Validation\Validator;

use App\Validation\ValidatorTest;
use App\Validation\Validator;

class ValidateDigitsOnlyTest extends ValidatorTest
{
    /**
     * Tests whether digits-only validation works.
     *
     * @return void
     */
    public function testValidateDigitsOnly()
    {
        $trans = $this->getTranslator();
        $trans->shouldReceive('trans')->andReturn('');
        $rules = ['foo' => ['digits_only']];

        $validator = new Validator($trans, ['foo' => '1234567890'], $rules);
        $this->assertTrue($validator->passes(), 'A string of digits and nothing else should pass validation');

        $validator = new Validator($trans, ['foo' => '12345.67890'], $rules);
        $this->assertFalse($validator->passes(), 'Decimal separators are not allowed');

        $validator = new Validator($trans, ['foo' => ' 1234567890'], $rules);
        $this->assertFalse($validator->passes(), 'Whitespace is not allowed in the string');

        $validator = new Validator($trans, ['foo' => '123,456,789'], $rules);
        $this->assertFalse($validator->passes(), 'Thousands separators are not allowed');
    }
}
