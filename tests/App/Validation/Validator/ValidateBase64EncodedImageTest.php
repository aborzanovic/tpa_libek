<?php

namespace App\Validation\Validator;

use App\Validation\ValidatorTest;
use App\Validation\Validator;

class ValidateBase64EncodedImageTest extends ValidatorTest
{
    /**
     * Tests whether base64-encoded image validation.
     *
     * @return void
     */
    public function testValidateBase64EncodedImage()
    {
        $trans = $this->getTranslator();
        $trans->shouldReceive('trans')->andReturn('');
        $rules = ['image' => ['base64_encoded_image']];

        $validator = new Validator($trans, ['image' => '!@#$%'], $rules);
        $this->assertFalse($validator->passes(), 'Not a valid base64-encoded string');

        $validator = new Validator($trans, ['image' => 'ZWFzdGVyIGVnZyE='], $rules);
        $this->assertFalse($validator->passes(), 'Not a valid image blob');

        /**
         * Image taken from the offical PHP documentation.
         *
         * @link http://php.net/manual/en/imagick.readimageblob.php
         */
        $validator = new Validator($trans, ['image' => 'iVBORw0KGgoAAAANSUhEUgAAAM0AAADNCAMAAAAsYgRbAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABJQTFRF3NSmzMewPxIG//ncJEJsldTou1jHgAAAARBJREFUeNrs2EEKgCAQBVDLuv+V20dENbMY831wKz4Y/VHb/5RGQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0NDQ0PzMWtyaGhoaGhoaGhoaGhoaGhoxtb0QGhoaGhoaGhoaGhoaGhoaMbRLEvv50VTQ9OTQ5OpyZ01GpM2g0bfmDQaL7S+ofFC6xv3ZpxJiywakzbvd9r3RWPS9I2+MWk0+kbf0Hih9Y17U0nTHibrDDQ0NDQ0NDQ0NDQ0NDQ0NTXbRSL/AK72o6GhoaGhoRlL8951vwsNDQ0NDQ1NDc0WyHtDTEhDQ0NDQ0NTS5MdGhoaGhoaGhoaGhoaGhoaGhoaGhoaGposzSHAAErMwwQ2HwRQAAAAAElFTkSuQmCC'], $rules);
        $this->assertTrue($validator->passes(), 'A valid base64-encoded image');
    }
}
