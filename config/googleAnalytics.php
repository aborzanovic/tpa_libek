<?php

return [

    /**
     * The Google Analytics tracking ID for this website.
     */
    'trackingId' => env('GOOGLE_ANALYTICS_TRACKING_ID', null),

    /**
     * The domain tracked for this website.
     *
     * This should be the `production` domain, as the Analytics code isn't
     * loaded in any other environment.
     *
     * It's usually safe to just leave this set to "auto".
     */
    'domain' => 'auto',

];
