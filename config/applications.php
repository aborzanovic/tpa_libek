<?php

return [
    /**
     * The deadline for submitting applications.
     */
    'deadline' => '2017-09-19 00:00:00',
];
